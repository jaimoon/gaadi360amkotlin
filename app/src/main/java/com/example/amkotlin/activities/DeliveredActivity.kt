package com.example.amkotlin.activities

import android.content.Intent
import android.graphics.Typeface
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.*
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.eightbitlab.bottomnavigationbar.BottomBarItem
import com.eightbitlab.bottomnavigationbar.BottomNavigationBar
import com.example.amkotlin.MainActivity
import com.example.amkotlin.R
import com.example.amkotlin.adapters.DeliveredHistoryAdapter
import com.example.amkotlin.models.DeliveredHistoryModel
import com.example.amkotlin.utils.AppUrls
import com.example.amkotlin.utils.NetworkChecking
import com.example.amkotlin.utils.UserSessionManager
import com.google.android.material.snackbar.Snackbar
import org.json.JSONArray
import org.json.JSONException
import java.util.*
import java.util.stream.Collectors
import java.util.stream.IntStream

class DeliveredActivity : AppCompatActivity(), View.OnClickListener {
    var close: ImageView? = null
    var no_history_img: ImageView? = null
    var toolbar_title: TextView? = null
    var no_history_txt: TextView? = null
    private var checkInternet = false
    var delivered_history_recyclerview: RecyclerView? = null
    var userSessionManager: UserSessionManager? = null
    var accessToken: String? = null
    var authority: String? = null
    var history_search: SearchView? = null

    /*History*/
    var deliveredHistoryAdapter: DeliveredHistoryAdapter? = null
    var deliveredHistoryModels: ArrayList<DeliveredHistoryModel> =
        ArrayList<DeliveredHistoryModel>()
    var regular: Typeface? = null
    var bold: Typeface? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_delivered)
        regular = Typeface.createFromAsset(assets, "proxima_nova_regular.otf")
        bold = Typeface.createFromAsset(assets, "proxima_nova_bold.otf")
        checkInternet = NetworkChecking.isConnected(this)
        userSessionManager = UserSessionManager(this)
        val userDetails: HashMap<String, String?>? = userSessionManager?.userDetails
        accessToken = userDetails?.get(UserSessionManager.KEY_ACCSES)
        authority = userDetails?.get(UserSessionManager.AUTHORITY)
        setupBottomBar()
        toolbar_title = findViewById(R.id.toolbar_title)
        toolbar_title?.setTypeface(bold)
        no_history_img = findViewById(R.id.no_history_img)
        no_history_txt = findViewById(R.id.no_history_txt)
        close = findViewById(R.id.close)
        close?.setOnClickListener(this)
        delivered_history_recyclerview =
            findViewById(R.id.delivered_history_recyclerview)
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(applicationContext)
        delivered_history_recyclerview?.setLayoutManager(layoutManager)
        deliveredHistoryAdapter = DeliveredHistoryAdapter(
            deliveredHistoryModels,
            this@DeliveredActivity,
            R.layout.row_delivery_history
        )
        history
        history_search = findViewById(R.id.history_search)
        val searchEditText = history_search?.findViewById<EditText>(androidx.appcompat.R.id.search_src_text)
        history_search?.setOnClickListener(View.OnClickListener { v: View? ->
            history_search?.setIconified(
                false
            )
        })
        history_search?.setOnQueryTextListener(object :
            SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

            override fun onQueryTextChange(query: String): Boolean {
                deliveredHistoryAdapter?.getFilter()?.filter(query)
                return false
            }
        })
    }

    private val history: Unit
        private get() {
            val url: String = AppUrls.BASE_URL + AppUrls.HISTORY.toString() + "3"
            val stringRequest: StringRequest =
                object : StringRequest(
                    Method.GET,
                    url,
                    Response.Listener { response ->
                        deliveredHistoryModels.clear()
                        try {
                            val jsonArray = JSONArray(response)
                            if (jsonArray.length() != 0) {
                                history_search!!.visibility = View.VISIBLE
                                no_history_img!!.visibility = View.GONE
                                no_history_txt!!.visibility = View.GONE
                                for (i in 0 until jsonArray.length()) {
                                    val jsonObject1 = jsonArray.getJSONObject(i)
                                    val rm = DeliveredHistoryModel()
                                    rm.bookingId=(jsonObject1.optString("bookingId"))
                                    rm.brandId=(jsonObject1.optString("brandId"))
                                    rm.brandName=(jsonObject1.optString("brandName"))
                                    rm.modelId=(jsonObject1.optString("modelId"))
                                    rm.modelName=(jsonObject1.optString("modelName"))
                                    rm.registrationNumber=(jsonObject1.optString("registrationNumber"))
                                    rm.bookingDate=(jsonObject1.optString("bookingDate"))
                                    rm.bookingTime=(jsonObject1.optString("bookingTime"))
                                    rm.finalPrice=(jsonObject1.optString("finalPrice"))
                                    rm.pickupAddress=(jsonObject1.optString("pickupAddress"))
                                    rm.status=(jsonObject1.optString("status"))
                                    rm.serviceCenterName=(jsonObject1.optString("serviceCenterName"))
                                    rm.paymentStatus=(jsonObject1.optString("paymentStatus"))
                                    rm.promocodeAmount=(jsonObject1.optString("promocodeAmount"))
                                    rm.isMovingCondition=(jsonObject1.optBoolean("movingCondition"))
                                    var services: String? = ""
                                    if (jsonObject1.has("services")) {
                                        val servicesArray =
                                            jsonObject1.optJSONArray("services")
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                            services = IntStream.range(
                                                0,
                                                servicesArray.length()
                                            ).mapToObj { j: Int ->
                                                try {
                                                    return@mapToObj servicesArray.getJSONObject(
                                                        j
                                                    ).optString("serviceName")
                                                } catch (e: JSONException) {
                                                    e.printStackTrace()
                                                    return@mapToObj ""
                                                }
                                            }.collect(Collectors.joining("\n"))
                                        }
                                        rm.services=(services)
                                    }
                                    deliveredHistoryModels.add(rm)
                                }
                                delivered_history_recyclerview!!.adapter =
                                    deliveredHistoryAdapter
                                deliveredHistoryAdapter?.notifyDataSetChanged()
                            } else {
                                history_search!!.visibility = View.GONE
                                no_history_txt!!.visibility = View.VISIBLE
                            }
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    },
                    Response.ErrorListener { error ->
                        if (error is TimeoutError || error is NoConnectionError) {
                        } else if (error is AuthFailureError) {
                        } else if (error is ServerError) {
                        } else if (error is NetworkError) {
                        } else if (error is ParseError) {
                        }
                    }
                ) {
                    override fun getHeaders(): Map<String, String> {
                        val headers: MutableMap<String, String> =
                            HashMap()
                        headers["Authorization"] = "Bearer $accessToken"
                        return headers
                    }
                }
            val requestQueue = Volley.newRequestQueue(this@DeliveredActivity)
            requestQueue.add(stringRequest)
        }

    override fun onClick(v: View) {
        if (v === close) {
            if (checkInternet) {
                val intent = Intent(this@DeliveredActivity, MainActivity::class.java)
                startActivity(intent)
            } else {
                val snackbar = Snackbar.make(
                    window.decorView,
                    "Check Internet Connection",
                    Snackbar.LENGTH_LONG
                )
                snackbar.show()
            }
        }
    }

    private fun setupBottomBar() {
        val bottomNavigationBar =
            findViewById<BottomNavigationBar>(R.id.bottom_bar)
        val home =
            BottomBarItem(R.drawable.ic_house_black_silhouette_without_door, R.string.newHistory)
        val history = BottomBarItem(R.drawable.ic_diploma, R.string.inProcessHistory)
        val settings =
            BottomBarItem(R.drawable.ic_diploma, R.string.deliveredHistory)
        bottomNavigationBar
            .addTab(home)
            .addTab(history)
            .addTab(settings)
        bottomNavigationBar.selectTab(2, true)
        bottomNavigationBar.setOnSelectListener { position -> showContent(position) }
    }

    fun showContent(position: Int) {
        if (position == 0) {
            val intent = Intent(this@DeliveredActivity, TodayHistoryActivity::class.java)
            startActivity(intent)
        }
        if (position == 1) {
            val intent = Intent(this@DeliveredActivity, UpcomingHistoryActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onBackPressed() {
        val intent = Intent(this@DeliveredActivity, MainActivity::class.java)
        startActivity(intent)
    }
}

