package com.example.amkotlin.activities

import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.view.View
import android.webkit.CookieManager
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.amkotlin.R
import com.example.amkotlin.utils.NetworkChecking
import com.google.android.material.snackbar.Snackbar
import com.taishi.flipprogressdialog.FlipProgressDialog
import java.util.*

class CancellationAndRefundPolicyActivity : AppCompatActivity(),
    View.OnClickListener {
    var close: ImageView? = null
    var toolbar_title: TextView? = null
    private var checkInternet = false
    var cancellation_web: WebView? = null

    /*ProgressDialog*/
    var imageList: MutableList<Int> = ArrayList()
    var flipProgressDialog: FlipProgressDialog? = null
    var regular: Typeface? = null
    var bold: Typeface? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cancellation_and_refund_policy)
        regular = Typeface.createFromAsset(assets, "proxima_nova_regular.otf")
        bold = Typeface.createFromAsset(assets, "proxima_nova_bold.otf")
        checkInternet = NetworkChecking.isConnected(this)
        imageList.add(R.mipmap.ic_launcher)
        flipProgressDialog = FlipProgressDialog()
        flipProgressDialog!!.setImageList(imageList)
        flipProgressDialog!!.setCanceledOnTouchOutside(true)
        flipProgressDialog!!.setDimAmount(0.8f) //0.0f
        flipProgressDialog!!.setBackgroundColor(Color.parseColor("#00FFFFFF"))
        flipProgressDialog!!.setBackgroundAlpha(0.2f)
        flipProgressDialog!!.setBorderStroke(0)
        flipProgressDialog!!.setBorderColor(-1)
        flipProgressDialog!!.setCornerRadius(16)
        flipProgressDialog!!.setImageSize(200)
        flipProgressDialog!!.setImageMargin(10)
        flipProgressDialog!!.setOrientation("rotationY")
        flipProgressDialog!!.setDuration(600)
        flipProgressDialog!!.setStartAngle(0.0f)
        flipProgressDialog!!.setEndAngle(180.0f)
        flipProgressDialog!!.setMinAlpha(0.0f)
        flipProgressDialog!!.setMaxAlpha(1.0f)
        flipProgressDialog!!.show(fragmentManager, "")
        //flipProgressDialog.dismiss();
        close = findViewById(R.id.close)
        close?.setOnClickListener(this)
        toolbar_title = findViewById(R.id.toolbar_title)
        toolbar_title?.setTypeface(bold)
        cancellation_web = findViewById(R.id.cancellation_web)
        cancellation_web?.setInitialScale(1)
        cancellation_web?.getSettings()?.javaScriptEnabled = true
        cancellation_web?.getSettings()?.loadWithOverviewMode = true
        cancellation_web?.getSettings()?.useWideViewPort = true
        cancellation_web?.getSettings()?.builtInZoomControls = true
        cancellation_web?.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY)
        cancellation_web?.setScrollbarFadingEnabled(false)
        cancellation_web?.setWebViewClient(WebViewClient())
        CookieManager.getInstance().setAcceptCookie(true)
        cancellation_web?.loadUrl("http://www.gaadi360.com/cancellation-policy.html")
        cancellation_web?.setWebViewClient(object : WebViewClient() {
            override fun onPageFinished(view: WebView, url: String) {
                flipProgressDialog!!.dismiss()
            }
        })
    }

    override fun onClick(v: View) {
        if (v === close) {
            if (checkInternet) {
                val intent = Intent(
                    this@CancellationAndRefundPolicyActivity,
                    SettingActivity::class.java
                )
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                startActivity(intent)
                finish()
            } else {
                val snackbar = Snackbar.make(
                    window.decorView,
                    "Check Internet Connection",
                    Snackbar.LENGTH_LONG
                )
                snackbar.show()
            }
        }
    }

    override fun onBackPressed() {
        if (checkInternet) {
            val intent =
                Intent(this@CancellationAndRefundPolicyActivity, SettingActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(intent)
            finish()
        } else {
            val snackbar = Snackbar.make(
                window.decorView,
                "Check Internet Connection",
                Snackbar.LENGTH_LONG
            )
            snackbar.show()
        }
    }
}