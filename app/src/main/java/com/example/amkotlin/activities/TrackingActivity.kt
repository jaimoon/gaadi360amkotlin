package com.example.amkotlin.activities

import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.*
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.amkotlin.MainActivity
import com.example.amkotlin.R
import com.example.amkotlin.adapters.AreaManagerNameAdapter
import com.example.amkotlin.adapters.TrackingAdapter
import com.example.amkotlin.models.TrackingModel
import com.example.amkotlin.utils.AppUrls
import com.example.amkotlin.utils.GlobalCalls
import com.example.amkotlin.utils.NetworkChecking
import com.example.amkotlin.utils.UserSessionManager
import com.google.android.material.snackbar.Snackbar
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class TrackingActivity : AppCompatActivity(), View.OnClickListener {
    private var checkInternet = false
    var close: ImageView? = null
    var toolbar_title: TextView? = null
    var name_txt: TextView? = null
    var date_txt: TextView? = null
    var submit_btn: Button? = null
    var regular: Typeface? = null
    var bold: Typeface? = null
    var userSessionManager: UserSessionManager? = null
    var track_search: SearchView? = null
    var track_recyclerview: RecyclerView? = null
    var areaManagerNameAdapter: AreaManagerNameAdapter? = null
    var trackingAdapter: TrackingAdapter? = null
    var trackingModels: ArrayList<TrackingModel> = ArrayList<TrackingModel>()
    var accessToken: String? = null
    var areaManagerId: String? = null
    var userName: String? = null
    var mobile: String? = null
    var dialog: AlertDialog? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tracking)
        checkInternet = NetworkChecking.isConnected(this)
        regular = Typeface.createFromAsset(assets, "proxima_nova_regular.otf")
        bold = Typeface.createFromAsset(assets, "proxima_nova_bold.otf")
        userSessionManager = UserSessionManager(this)
        val userDetails: HashMap<String, String?>? = userSessionManager?.userDetails
        accessToken = userDetails?.get(UserSessionManager.KEY_ACCSES)
        close = findViewById(R.id.close)
        close?.setOnClickListener(this)
        toolbar_title = findViewById(R.id.toolbar_title)
        toolbar_title?.setTypeface(bold)
        name_txt = findViewById(R.id.name_txt)
        name_txt?.setTypeface(bold)
        name_txt?.setOnClickListener(this)
        date_txt = findViewById(R.id.date_txt)
        date_txt?.setTypeface(bold)
        date_txt?.setOnClickListener(this)
        submit_btn = findViewById(R.id.submit_btn)
        submit_btn?.setTypeface(bold)
        submit_btn?.setOnClickListener(this)
        track_recyclerview = findViewById(R.id.track_recyclerview)
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(applicationContext)
        track_recyclerview?.setLayoutManager(layoutManager)
        trackingAdapter =
            TrackingAdapter(trackingModels, this@TrackingActivity, R.layout.row_tracking)
        details
        track_search = findViewById(R.id.track_search)
        val searchEditText =
            track_search?.findViewById<EditText>(androidx.appcompat.R.id.search_src_text)
        track_search?.setOnClickListener(View.OnClickListener { v: View? ->
            track_search?.setIconified(
                false
            )
        })
        track_search?.setOnQueryTextListener(object :
            SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

            override fun onQueryTextChange(query: String): Boolean {
                trackingAdapter?.getFilter()?.filter(query)
                return false
            }
        })
    }

    private val details: Unit
        private get() {
            val url: String = AppUrls.BASE_URL + AppUrls.AREA_MANAGER_LIST
            val stringRequest: StringRequest =
                object : StringRequest(
                    Method.GET,
                    url,
                    Response.Listener { response ->
                        trackingModels.clear()
                        try {
                            val jsonObject = JSONObject(response)
                            val status = jsonObject.getString("status")
                            val message = jsonObject.getString("message")
                            if (status.equals("2000", ignoreCase = true)) {
                                val jsonObject1 = jsonObject.optJSONObject("data")
                                val jsonArray =
                                    jsonObject1.optJSONArray("areaManagerLocation")
                                if (jsonArray.length() != 0) {
                                    for (i in 0 until jsonArray.length()) {
                                        val jsonObject2 = jsonArray.optJSONObject(i)
                                        val tm = TrackingModel()
                                        tm.id=(jsonObject2.optString("id"))
                                        tm.userId=(jsonObject2.optString("userId"))
                                        tm.setCreatedTime(jsonObject2.optString("createdTime"))
                                        tm.latitude=(jsonObject2.optString("latitude"))
                                        tm.longitude=(jsonObject2.optString("longitude"))
                                        tm.location=(jsonObject2.optString("location"))
                                        userName = jsonObject2.optString("userName")
                                        mobile = jsonObject2.optString("mobile")
                                        tm.userName=(userName)
                                        tm.mobile=(mobile)
                                        trackingModels.add(tm)
                                    }
                                    track_recyclerview!!.adapter = trackingAdapter
                                    trackingAdapter?.notifyDataSetChanged()
                                } else {
                                    GlobalCalls.showToast(
                                        "No Data Available",
                                        this@TrackingActivity
                                    )
                                }
                            }
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    },
                    Response.ErrorListener { error ->
                        if (error is TimeoutError || error is NoConnectionError) {
                        } else if (error is AuthFailureError) {
                        } else if (error is ServerError) {
                        } else if (error is NetworkError) {
                        } else if (error is ParseError) {
                        }
                    }
                ) {
                    override fun getHeaders(): Map<String, String> {
                        val headers: MutableMap<String, String> =
                            HashMap()
                        headers["Authorization"] = "Bearer $accessToken"
                        return headers
                    }
                }
            val requestQueue = Volley.newRequestQueue(this@TrackingActivity)
            requestQueue.add(stringRequest)
        }

    override fun onClick(v: View) {
        if (v === close) {
            if (checkInternet) {
                val intent = Intent(this@TrackingActivity, MainActivity::class.java)
                startActivity(intent)
            } else {
                val snackbar = Snackbar.make(
                    window.decorView,
                    "Check Internet Connection",
                    Snackbar.LENGTH_LONG
                )
                snackbar.show()
            }
        }
        if (v === name_txt) {
            if (checkInternet) {

                //name_txt.setTextColor(ContextCompat.getColor(TrackingActivity.this, R.color.black));
                //name_txt.setBackgroundResource(R.drawable.rounded_border);
                names
            } else {
                val snackbar = Snackbar.make(
                    window.decorView,
                    "Check Internet Connection",
                    Snackbar.LENGTH_LONG
                )
                snackbar.show()
            }
        }
        if (v === date_txt) {
            if (checkInternet) {

                //date_txt.setTextColor(ContextCompat.getColor(TrackingActivity.this, R.color.black));
                //date_txt.setBackgroundResource(R.drawable.rounded_border);
                date
            } else {
                val snackbar = Snackbar.make(
                    window.decorView,
                    "Check Internet Connection",
                    Snackbar.LENGTH_LONG
                )
                snackbar.show()
            }
        }
        if (v === submit_btn) {
            if (checkInternet) {
                specificDetails
            } else {
                val snackbar = Snackbar.make(
                    window.decorView,
                    "Check Internet Connection",
                    Snackbar.LENGTH_LONG
                )
                snackbar.show()
            }
        }
    }

    private val names: Unit
        private get() {
            val url: String = AppUrls.BASE_URL + AppUrls.AREA_MANAGER_LIST
            val stringRequest: StringRequest =
                object : StringRequest(
                    Method.GET,
                    url,
                    Response.Listener { response ->
                        trackingModels.clear()
                        try {
                            val jsonObject = JSONObject(response)
                            val status = jsonObject.getString("status")
                            val message = jsonObject.getString("message")
                            if (status.equals("2000", ignoreCase = true)) {
                                val jsonObject1 = jsonObject.optJSONObject("data")
                                val jsonArray =
                                    jsonObject1.optJSONArray("areaManagerLocation")
                                if (jsonArray.length() != 0) {
                                    for (i in 0 until jsonArray.length()) {
                                        val jsonObject2 = jsonArray.optJSONObject(i)
                                        val tm = TrackingModel()
                                        tm.id=(jsonObject2.optString("id"))
                                        tm.userId=(jsonObject2.optString("userId"))
                                        tm.setCreatedTime(jsonObject2.optString("createdTime"))
                                        tm.latitude=(jsonObject2.optString("latitude"))
                                        tm.longitude=(jsonObject2.optString("longitude"))
                                        tm.location=(jsonObject2.optString("location"))
                                        userName = jsonObject2.optString("userName")
                                        mobile = jsonObject2.optString("mobile")
                                        tm.userName=(userName)
                                        tm.mobile=(mobile)
                                        trackingModels.add(tm)
                                    }
                                    track_recyclerview!!.adapter = trackingAdapter
                                    trackingAdapter?.notifyDataSetChanged()
                                    areaManagerNameDialog()
                                } else {
                                    GlobalCalls.showToast(
                                        "No Data Available",
                                        this@TrackingActivity
                                    )
                                }
                            }
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    },
                    Response.ErrorListener { error ->
                        if (error is TimeoutError || error is NoConnectionError) {
                        } else if (error is AuthFailureError) {
                        } else if (error is ServerError) {
                        } else if (error is NetworkError) {
                        } else if (error is ParseError) {
                        }
                    }
                ) {
                    override fun getHeaders(): Map<String, String> {
                        val headers: MutableMap<String, String> =
                            HashMap()
                        headers["Authorization"] = "Bearer $accessToken"
                        return headers
                    }
                }
            val requestQueue = Volley.newRequestQueue(this@TrackingActivity)
            requestQueue.add(stringRequest)
        }

    private fun areaManagerNameDialog() {
        val builder =
            AlertDialog.Builder(this@TrackingActivity)
        val inflater = this@TrackingActivity.layoutInflater
        val dialog_layout: View =
            inflater.inflate(R.layout.area_manager_name_dialog, null)
        val name_title = dialog_layout.findViewById<TextView>(R.id.name_title)
        name_title.setTypeface(regular)
        val name_search: SearchView =
            dialog_layout.findViewById(R.id.name_search)
        val searchEditText =
            name_search.findViewById<EditText>(androidx.appcompat.R.id.search_src_text)
        val name_recyclerview: RecyclerView = dialog_layout.findViewById(R.id.name_recyclerview)
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(applicationContext)
        name_recyclerview.layoutManager = layoutManager
        areaManagerNameAdapter =
            AreaManagerNameAdapter(trackingModels, this@TrackingActivity, R.layout.row_names)
        name_recyclerview.adapter = areaManagerNameAdapter
        name_search.setOnClickListener { v: View? ->
            name_search.isIconified = false
        }
        builder.setView(dialog_layout).setNegativeButton(
            "CANCEL"
        ) { dialogInterface: DialogInterface, i: Int -> dialogInterface.dismiss() }
        dialog = builder.create()
        name_search.setOnQueryTextListener(object :
            SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

            override fun onQueryTextChange(query: String): Boolean {
                areaManagerNameAdapter?.getFilter()?.filter(query)
                return false
            }
        })
        dialog!!.show()
    }

    fun setAreaManagerName(
        id: String?,
        name: String?,
        areamobile: String?
    ) {
        dialog!!.dismiss()
        areaManagerNameAdapter?.notifyDataSetChanged()
        areaManagerId = id
        userName = name
        mobile = areamobile
        name_txt!!.text = name
        name_txt!!.setTextColor(ContextCompat.getColor(this@TrackingActivity, R.color.white))
        name_txt!!.setBackgroundResource(R.drawable.rounded_border_btn)
        date
    }

    val date: Unit
        get() {
            val calendar = Calendar.getInstance()
            val day = calendar[Calendar.DAY_OF_MONTH]
            val month = calendar[Calendar.MONTH]
            val year = calendar[Calendar.YEAR]
            val datePickerDialog = DatePickerDialog(
                this,
                OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    val strDate =
                        dayOfMonth.toString() + "-" + (monthOfYear + 1) + "-" + year
                    date_txt!!.text = strDate
                    date_txt!!.setTextColor(
                        ContextCompat.getColor(
                            this@TrackingActivity,
                            R.color.white
                        )
                    )
                    date_txt!!.setBackgroundResource(R.drawable.rounded_border_btn)
                }, year, month, day
            )
            datePickerDialog.setButton(
                DialogInterface.BUTTON_NEGATIVE,
                "Cancel"
            ) { dialog, which -> }
            datePickerDialog.datePicker.maxDate = System.currentTimeMillis()
            datePickerDialog.show()
        }

    //tm.setUserName(jsonObject2.optString("userName"));
    //tm.setMobile(jsonObject2.optString("mobile"));
    private val specificDetails: Unit
        private get() {
            val url: String =
                AppUrls.BASE_URL + AppUrls.AREA_MANAGER_LOCATION_DETAILS.toString() + areaManagerId + AppUrls.AREA_MANAGER_LOCATION_DETAILS2.toString() +
                        date_txt!!.text.toString()
            Log.d("URL", url)
            val stringRequest: StringRequest =
                object : StringRequest(
                    Method.GET,
                    url,
                    Response.Listener { response ->
                        trackingModels.clear()
                        try {
                            val jsonObject = JSONObject(response)
                            val status = jsonObject.getString("status")
                            val message = jsonObject.getString("message")
                            if (status.equals("2000", ignoreCase = true)) {
                                val jsonObject1 = jsonObject.optJSONObject("data")
                                val jsonArray =
                                    jsonObject1.optJSONArray("areaManagerLocation")
                                if (jsonArray.length() != 0) {
                                    for (i in 0 until jsonArray.length()) {
                                        val jsonObject2 = jsonArray.optJSONObject(i)
                                        val tm = TrackingModel()
                                        tm.id=(jsonObject2.optString("id"))
                                        tm.userId=(jsonObject2.optString("userId"))
                                        tm.setCreatedTime(jsonObject2.optString("createdTime"))
                                        tm.latitude=(jsonObject2.optString("latitude"))
                                        tm.longitude=(jsonObject2.optString("longitude"))
                                        tm.location=(jsonObject2.optString("location"))
                                        tm.userName=(userName)
                                        tm.mobile=(mobile)
                                        trackingModels.add(tm)
                                    }
                                    track_recyclerview!!.adapter = trackingAdapter
                                    trackingAdapter?.notifyDataSetChanged()
                                } else {
                                    GlobalCalls.showToast(
                                        "No Data Available",
                                        this@TrackingActivity
                                    )
                                }
                            }
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    },
                    Response.ErrorListener { error ->
                        if (error is TimeoutError || error is NoConnectionError) {
                        } else if (error is AuthFailureError) {
                        } else if (error is ServerError) {
                        } else if (error is NetworkError) {
                        } else if (error is ParseError) {
                        }
                    }
                ) {
                    override fun getHeaders(): Map<String, String> {
                        val headers: MutableMap<String, String> =
                            HashMap()
                        headers["Authorization"] = "Bearer $accessToken"
                        return headers
                    }
                }
            val requestQueue = Volley.newRequestQueue(this@TrackingActivity)
            requestQueue.add(stringRequest)
        }
}

