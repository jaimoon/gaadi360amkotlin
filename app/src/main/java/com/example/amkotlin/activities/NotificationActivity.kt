package com.example.amkotlin.activities

import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import cn.pedant.SweetAlert.SweetAlertDialog
import com.android.volley.*
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.amkotlin.MainActivity
import com.example.amkotlin.R
import com.example.amkotlin.adapters.NotificationAdapter
import com.example.amkotlin.models.NotificationModel
import com.example.amkotlin.utils.AppUrls
import com.example.amkotlin.utils.NetworkChecking
import com.example.amkotlin.utils.UserSessionManager
import com.google.android.material.snackbar.Snackbar
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class NotificationActivity : AppCompatActivity(), View.OnClickListener {
    private var checkInternet = false
    var close: ImageView? = null
    var no_data_img: ImageView? = null
    var toolbar_title: TextView? = null
    var clear_txt: TextView? = null
    var notification_recyclerview: RecyclerView? = null
    var regular: Typeface? = null
    var bold: Typeface? = null
    var sweetAlertDialog: SweetAlertDialog? = null
    var userSessionManager: UserSessionManager? = null
    var accessToken: String? = null
    var notificationAdapter: NotificationAdapter? = null
    var notificationModels: ArrayList<NotificationModel> =
        ArrayList<NotificationModel>()
    var dialogBuilder: AlertDialog.Builder? = null
    var alertDialog: AlertDialog? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notification)
        userSessionManager = UserSessionManager(this)
        val userDetails: HashMap<String, String?>? = userSessionManager?.userDetails
        accessToken = userDetails?.get(UserSessionManager.KEY_ACCSES)
        regular = Typeface.createFromAsset(assets, "proxima_nova_regular.otf")
        bold = Typeface.createFromAsset(assets, "proxima_nova_bold.otf")
        toolbar_title = findViewById(R.id.toolbar_title)
        toolbar_title?.setTypeface(bold)
        clear_txt = findViewById(R.id.clear_txt)
        clear_txt?.setTypeface(bold)
        clear_txt?.setOnClickListener(this)
        checkInternet = NetworkChecking.isConnected(this)
        close = findViewById(R.id.close)
        close?.setOnClickListener(this)
        no_data_img = findViewById(R.id.no_data_img)
        notification_recyclerview = findViewById(R.id.notification_recyclerview)
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(applicationContext)
        notification_recyclerview?.setLayoutManager(layoutManager)
        notificationAdapter = NotificationAdapter(
            notificationModels,
            this@NotificationActivity,
            R.layout.row_notification
        )
        notification
    }

    val notification: Unit
        get() {
            val url: String = AppUrls.BASE_URL + AppUrls.NOTIFICATIONS
            val stringRequest: StringRequest =
                object : StringRequest(
                    Method.GET,
                    url,
                    Response.Listener { response ->
                        notificationModels.clear()
                        try {
                            val jsonArray = JSONArray(response)
                            if (jsonArray.length() != 0) {
                                clear_txt!!.visibility = View.VISIBLE
                                no_data_img!!.visibility = View.GONE
                                for (i in 0 until jsonArray.length()) {
                                    val jsonObject = jsonArray.getJSONObject(i)
                                    val nm = NotificationModel()
                                    nm.notificationId=jsonObject.getString("notificationId")
                                    nm.userId=jsonObject.getString("userId")
                                    nm.bookingId=jsonObject.getString("bookingId")
                                    nm.description=jsonObject.getString("description")
                                    nm.deleted=jsonObject.getString("deleted")
                                    nm.readFlag=jsonObject.getString("readFlag")
                                    nm.createdTime=jsonObject.getString("createdTime")
                                    nm.modifiedTime=jsonObject.getString("modifiedTime")
                                    notificationModels.add(nm)
                                }
                                notification_recyclerview!!.adapter = notificationAdapter
                                notificationAdapter?.notifyDataSetChanged()
                            } else {
                                clear_txt!!.visibility = View.GONE
                                no_data_img!!.visibility = View.VISIBLE
                            }
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    },
                    Response.ErrorListener { error ->
                        if (error is TimeoutError || error is NoConnectionError) {
                        } else if (error is AuthFailureError) {
                        } else if (error is ServerError) {
                        } else if (error is NetworkError) {
                        } else if (error is ParseError) {
                        }
                    }
                ) {
                    override fun getHeaders(): Map<String, String> {
                        val headers: MutableMap<String, String> =
                            HashMap()
                        headers["Authorization"] = "Bearer $accessToken"
                        return headers
                    }
                }
            val requestQueue = Volley.newRequestQueue(this@NotificationActivity)
            requestQueue.add(stringRequest)
        }

    override fun onClick(v: View) {
        if (v === close) {
            if (checkInternet) {
                val intent = Intent(this@NotificationActivity, MainActivity::class.java)
                startActivity(intent)
            } else {
                val snackbar = Snackbar.make(
                    window.decorView,
                    "Check Internet Connection",
                    Snackbar.LENGTH_LONG
                )
                snackbar.show()
            }
        }
        if (v === clear_txt) {
            if (checkInternet) {
                showClearDialog(R.layout.warning_dialog)

            } else {
                val snackbar = Snackbar.make(
                    window.decorView,
                    "Check Internet Connection",
                    Snackbar.LENGTH_LONG
                )
                snackbar.show()
            }
        }
    }

    private fun clearNotifications() {
        val url: String = AppUrls.BASE_URL + AppUrls.CLEAR_NOTIFICATIONS
        val stringRequest: StringRequest =
            object : StringRequest(
                Method.DELETE,
                url,
                Response.Listener { response ->
                    try {
                        val jsonObject = JSONObject(response)
                        val status = jsonObject.optString("status")
                        val code = jsonObject.optString("code")
                        val message = jsonObject.optString("message")
                        notification
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                },
                Response.ErrorListener { error ->
                    if (error is TimeoutError || error is NoConnectionError) {
                    } else if (error is AuthFailureError) {
                    } else if (error is ServerError) {
                    } else if (error is NetworkError) {
                    } else if (error is ParseError) {
                    }
                }
            ) {
                override fun getHeaders(): Map<String, String> {
                    val headers: MutableMap<String, String> =
                        HashMap()
                    headers["Authorization"] = "Bearer $accessToken"
                    return headers
                }
            }
        val requestQueue = Volley.newRequestQueue(this@NotificationActivity)
        requestQueue.add(stringRequest)
    }

    private fun showClearDialog(layout: Int) {
        dialogBuilder = AlertDialog.Builder(this@NotificationActivity)
        val layoutView = layoutInflater.inflate(layout, null)
        val title_txt = layoutView.findViewById<TextView>(R.id.title_txt)
        title_txt.setTypeface(bold)
        title_txt.text = "Are you sure?"
        val msg_txt = layoutView.findViewById<TextView>(R.id.msg_txt)
        msg_txt.setTypeface(regular)
        msg_txt.text = "You want to Clear All !"
        val ok_btn =
            layoutView.findViewById<Button>(R.id.ok_btn)
        ok_btn.setTypeface(bold)
        ok_btn.text = "Yes, Clear"
        val cancel_btn =
            layoutView.findViewById<Button>(R.id.cancel_btn)
        cancel_btn.setTypeface(bold)
        cancel_btn.text = "No, Keep It"
        dialogBuilder!!.setView(layoutView)
        alertDialog = dialogBuilder!!.create()
        alertDialog!!.window!!.attributes.windowAnimations = R.style.DialogAnimation
        alertDialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        alertDialog!!.show()
        ok_btn.setOnClickListener {
            alertDialog!!.dismiss()
            clearNotifications()
        }
        cancel_btn.setOnClickListener { alertDialog!!.dismiss() }
        alertDialog!!.setCancelable(false)
    }

    override fun onBackPressed() {
        val intent = Intent(this@NotificationActivity, MainActivity::class.java)
        startActivity(intent)
    }
}

