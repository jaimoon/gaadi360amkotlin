package com.example.amkotlin.activities

import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.*
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.eightbitlab.bottomnavigationbar.BottomBarItem
import com.eightbitlab.bottomnavigationbar.BottomNavigationBar
import com.example.amkotlin.MainActivity
import com.example.amkotlin.R
import com.example.amkotlin.adapters.ConcernAdapter
import com.example.amkotlin.models.ConcernModel
import com.example.amkotlin.utils.AppUrls
import com.example.amkotlin.utils.NetworkChecking
import com.example.amkotlin.utils.UserSessionManager
import com.google.android.material.snackbar.Snackbar
import org.json.JSONArray
import org.json.JSONException
import java.util.*

class ConcernListActivity : AppCompatActivity(), View.OnClickListener {
    private var checkInternet = false
    var close: ImageView? = null
    var no_data_img: ImageView? = null
    var toolbar_title: TextView? = null
    var concern_recyclerview: RecyclerView? = null
    var regular: Typeface? = null
    var bold: Typeface? = null
    var userSessionManager: UserSessionManager? = null
    var accessToken: String? = null
    var concern_search: SearchView? = null
    var concernAdapter: ConcernAdapter? = null
    var concernModels: ArrayList<ConcernModel> = ArrayList<ConcernModel>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_concern_list)
        userSessionManager = UserSessionManager(this)
        val userDetails: HashMap<String, String?>? = userSessionManager?.userDetails
        accessToken = userDetails?.get(UserSessionManager.KEY_ACCSES)
        regular = Typeface.createFromAsset(assets, "proxima_nova_regular.otf")
        bold = Typeface.createFromAsset(assets, "proxima_nova_bold.otf")
        setupBottomBar()
        toolbar_title = findViewById(R.id.toolbar_title)
        toolbar_title?.setTypeface(bold)
        checkInternet = NetworkChecking.isConnected(this)
        close = findViewById(R.id.close)
        close?.setOnClickListener(this)
        no_data_img = findViewById(R.id.no_data_img)
        concern_recyclerview = findViewById(R.id.concern_recyclerview)
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(applicationContext)
        concern_recyclerview?.setLayoutManager(layoutManager)
        concernAdapter =
            ConcernAdapter(concernModels, this@ConcernListActivity, R.layout.row_concern)
        concerns
        concern_search = findViewById(R.id.concern_search)
        val searchEditText =
            concern_search?.findViewById<EditText>(androidx.appcompat.R.id.search_src_text)
        concern_search?.setOnClickListener(View.OnClickListener { v: View? ->
            concern_search?.setIconified(
                false
            )
        })
        concern_search?.setOnQueryTextListener(object :
            SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

            override fun onQueryTextChange(query: String): Boolean {
                concernAdapter?.getFilter()?.filter(query)
                return false
            }
        })
    }

    private val concerns: Unit
        private get() {
            val url = AppUrls.BASE_URL + AppUrls.CONCERNS + "1"
            val stringRequest: StringRequest =
                object : StringRequest(
                    Method.GET,
                    url,
                    Response.Listener { response ->
                        concernModels.clear()
                        try {
                            val jsonArray = JSONArray(response)
                            if (jsonArray.length() != 0) {
                                no_data_img!!.visibility = View.GONE
                                for (i in 0 until jsonArray.length()) {
                                    val jsonObject = jsonArray.optJSONObject(i)
                                    val cm = ConcernModel()
                                    cm.serviceCenterName=(jsonObject.optString("serviceCenterName"))
                                    cm.concernId=(jsonObject.optString("concernId"))
                                    cm.bookingId=(jsonObject.optString("bookingId"))
                                    cm.concernMessage=(jsonObject.optString("concernMessage"))
                                    cm.bookingDate=(jsonObject.optString("bookingDate"))
                                    cm.bookingTime=(jsonObject.optString("bookingTime"))
                                    cm.deliveredTime=(jsonObject.optString("deliveredTime"))
                                    cm.concernTime=(jsonObject.optString("concernTime"))
                                    cm.userMobile=(jsonObject.optString("userMobile"))
                                    cm.userName=(jsonObject.optString("userName"))
                                    cm.finalPrice=(jsonObject.optString("finalPrice"))
                                    cm.status=(jsonObject.optString("status"))
                                    cm.resolvedMessage=(jsonObject.optString("resolvedMessage"))
                                    concernModels.add(cm)
                                }
                                concern_recyclerview!!.adapter = concernAdapter
                                concernAdapter?.notifyDataSetChanged()
                            } else {
                                no_data_img!!.visibility = View.VISIBLE
                            }
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    },
                    Response.ErrorListener { error ->
                        if (error is TimeoutError || error is NoConnectionError) {
                        } else if (error is AuthFailureError) {
                        } else if (error is ServerError) {
                        } else if (error is NetworkError) {
                        } else if (error is ParseError) {
                        }
                    }
                ) {
                    override fun getHeaders(): Map<String, String> {
                        val headers: MutableMap<String, String> =
                            HashMap()
                        headers["Authorization"] = "Bearer $accessToken"
                        return headers
                    }
                }
            val requestQueue = Volley.newRequestQueue(this@ConcernListActivity)
            requestQueue.add(stringRequest)
        }

    override fun onClick(v: View) {
        if (v === close) {
            if (checkInternet) {
                val intent = Intent(this@ConcernListActivity, MainActivity::class.java)
                startActivity(intent)
            } else {
                val snackbar = Snackbar.make(
                    window.decorView,
                    "Check Internet Connection",
                    Snackbar.LENGTH_LONG
                )
                snackbar.show()
            }
        }
    }

    private fun setupBottomBar() {
        val bottomNavigationBar =
            findViewById<BottomNavigationBar>(R.id.bottom_bar)
        val home =
            BottomBarItem(R.drawable.ic_house_black_silhouette_without_door, R.string.open)
        val history = BottomBarItem(R.drawable.ic_diploma, R.string.closed)
        bottomNavigationBar
            .addTab(home)
            .addTab(history)
        bottomNavigationBar.selectTab(0, true)
        bottomNavigationBar.setOnSelectListener { position -> showContent(position) }
    }

    fun showContent(position: Int) {
        if (position == 1) {
            val intent =
                Intent(this@ConcernListActivity, ClosedConcernListActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onBackPressed() {
        val intent = Intent(this@ConcernListActivity, MainActivity::class.java)
        startActivity(intent)
    }
}
