package com.example.amkotlin.activities

import android.content.Intent
import android.graphics.*
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TableRow
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener
import com.android.volley.*
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.amkotlin.MainActivity
import com.example.amkotlin.R
import com.example.amkotlin.adapters.AddonAdapter
import com.example.amkotlin.adapters.InspAdapter
import com.example.amkotlin.adapters.PhotosAdapter
import com.example.amkotlin.adapters.ServicesAdapter
import com.example.amkotlin.models.BookingServicesModel
import com.example.amkotlin.models.InspectionModel
import com.example.amkotlin.utils.AppUrls
import com.example.amkotlin.utils.NetworkChecking
import com.example.amkotlin.utils.UserSessionManager
import com.google.android.material.snackbar.Snackbar
import org.json.JSONException
import org.json.JSONObject
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class BookingDetailActivity : AppCompatActivity(), View.OnClickListener,
    OnRefreshListener {
    var close: ImageView? = null
    var redirect_img: ImageView? = null
    private var checkInternet = false
    var bookingId: String? = null
    var accessToken: String? = null
    var authority: String? = null
    var brandId: String? = null
    var brandName: String? = null
    var modelId: String? = null
    var modelName: String? = null
    var status: String? = null
    var startRange: String? = null
    var endRange: String? = null
    var mobile: String? = null
    var sstMobileNumber: String? = null
    var serviceCenterId: String? = null
    var userId: String? = null
    var activity: String? = null
    var pickupAddressLatitude: String? = null
    var pickupAddressLongitude: String? = null
    var userSessionManager: UserSessionManager? = null
    var toolbar_title: TextView? = null
    var sst_name_txt: TextView? = null
    var booking_txt: TextView? = null
    var brand_txt: TextView? = null
    var model_txt: TextView? = null
    var date_txt: TextView? = null
    var time_txt: TextView? = null
    var addon_service_txt: TextView? = null
    var pick_date_txt: TextView? = null
    var pick_time_txt: TextView? = null
    var photos_txt: TextView? = null
    var payment_status_txt: TextView? = null
    var pickup_txt: TextView? = null
    var final_price_txt: TextView? = null
    var bike_txt: TextView? = null
    var service_txt: TextView? = null
    var address_txt: TextView? = null
    var est_price_txt: TextView? = null
    var est_txt: TextView? = null
    var tot_txt: TextView? = null
    var init_txt: TextView? = null
    var init_price_txt: TextView? = null
    var disc_txt: TextView? = null
    var dicount_txt: TextView? = null
    var addon_txt: TextView? = null
    var addon_price_txt: TextView? = null
    var insp_txt: TextView? = null
    var remarks_txt: TextView? = null
    var remark_txt: TextView? = null
    var moving_txt: TextView? = null
    var kilometers_txt: TextView? = null
    var customer_mobile_txt: TextView? = null
    var reg_txt: TextView? = null
    var customer_txt: TextView? = null
    var sst_txt: TextView? = null
    var sst_mobile_txt: TextView? = null
    var regular: Typeface? = null
    var bold: Typeface? = null
    var discount_tr: TableRow? = null
    var addon_tr: TableRow? = null
    var services_rl: RelativeLayout? = null
    var loc_rl: RelativeLayout? = null
    var photo_rl: RelativeLayout? = null
    var addon_rl: RelativeLayout? = null
    var insp_rl: RelativeLayout? = null
    var remarks_rl: RelativeLayout? = null
    var services_recyclerview: RecyclerView? = null
    var addon_recyclerview: RecyclerView? = null
    var photos_recyclerview: RecyclerView? = null
    var insp_recyclerview: RecyclerView? = null
    var servicesAdapter: ServicesAdapter? = null
    var servicesModels: MutableList<BookingServicesModel> =
        ArrayList<BookingServicesModel>()
    var addonAdapter: AddonAdapter? = null
    var addOnModels: MutableList<BookingServicesModel> =
        ArrayList<BookingServicesModel>()
    var inspAdapter: InspAdapter? = null
    var inspectionModels: MutableList<InspectionModel> =
        ArrayList<InspectionModel>()
    var photosAdapter: PhotosAdapter? = null
    var mylist: MutableList<String> = ArrayList()
    private var swipe: SwipeRefreshLayout? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_booking_detail)
        regular = Typeface.createFromAsset(assets, "proxima_nova_regular.otf")
        bold = Typeface.createFromAsset(assets, "proxima_nova_bold.otf")
        checkInternet = NetworkChecking.isConnected(this)
        userSessionManager = UserSessionManager(this)
        val userDetails: HashMap<String, String?>? = userSessionManager?.userDetails
        accessToken = userDetails?.get(UserSessionManager.KEY_ACCSES)
        authority = userDetails?.get(UserSessionManager.AUTHORITY)
        activity = intent.getStringExtra("activity")
        bookingId = intent.getStringExtra("bookingId")
        toolbar_title = findViewById(R.id.toolbar_title)
        toolbar_title?.setTypeface(bold)
        close = findViewById(R.id.close)
        close?.setOnClickListener(this)
        swipe = findViewById(R.id.swipe)
        swipe?.setColorSchemeResources(R.color.orange, R.color.colorPrimary)
        swipe?.setOnRefreshListener(this)
        redirect_img = findViewById(R.id.redirect_img)
        redirect_img?.setOnClickListener(this)
        sst_name_txt = findViewById(R.id.sst_name_txt)
        sst_name_txt?.setTypeface(bold)
        sst_name_txt?.setTextColor(Color.parseColor("#003171"))
        booking_txt = findViewById(R.id.booking_txt)
        booking_txt?.setTypeface(bold)
        customer_txt = findViewById(R.id.customer_txt)
        customer_txt?.setTypeface(bold)
        customer_mobile_txt = findViewById(R.id.customer_mobile_txt)
        customer_mobile_txt?.setTypeface(bold)
        customer_mobile_txt?.setPaintFlags(customer_mobile_txt?.getPaintFlags()?.or(Paint.UNDERLINE_TEXT_FLAG)!!)
        customer_mobile_txt?.setOnClickListener(this)
        sst_txt = findViewById(R.id.sst_txt)
        sst_txt?.setTypeface(bold)
        sst_mobile_txt = findViewById(R.id.sst_mobile_txt)
        sst_mobile_txt?.setTypeface(bold)
        sst_mobile_txt?.setPaintFlags(sst_mobile_txt?.getPaintFlags()?.or(Paint.UNDERLINE_TEXT_FLAG)!!)
        sst_mobile_txt?.setOnClickListener(this)
        bike_txt = findViewById(R.id.bike_txt)
        bike_txt?.setTypeface(bold)
        brand_txt = findViewById(R.id.brand_txt)
        brand_txt?.setTypeface(regular)
        model_txt = findViewById(R.id.model_txt)
        model_txt?.setTypeface(regular)
        reg_txt = findViewById(R.id.reg_txt)
        reg_txt?.setTypeface(regular)
        date_txt = findViewById(R.id.date_txt)
        date_txt?.setTypeface(regular)
        time_txt = findViewById(R.id.time_txt)
        time_txt?.setTypeface(regular)
        service_txt = findViewById(R.id.service_txt)
        service_txt?.setTypeface(bold)
        remarks_txt = findViewById(R.id.remarks_txt)
        remarks_txt?.setTypeface(bold)
        remark_txt = findViewById(R.id.remark_txt)
        remark_txt?.setTypeface(regular)
        addon_service_txt = findViewById(R.id.addon_service_txt)
        addon_service_txt?.setTypeface(bold)
        services_recyclerview = findViewById(R.id.services_recyclerview)
        val layoutManager2: RecyclerView.LayoutManager =
            LinearLayoutManager(applicationContext)
        services_recyclerview?.setLayoutManager(layoutManager2)
        servicesAdapter =
            ServicesAdapter(servicesModels, this@BookingDetailActivity, R.layout.row_addon)
        insp_recyclerview = findViewById(R.id.insp_recyclerview)
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(applicationContext)
        insp_recyclerview?.setLayoutManager(layoutManager)
        inspAdapter = InspAdapter(inspectionModels, this@BookingDetailActivity, R.layout.row_insp)
        addon_recyclerview = findViewById(R.id.addon_recyclerview)
        val layoutManager1: RecyclerView.LayoutManager =
            LinearLayoutManager(applicationContext)
        addon_recyclerview?.setLayoutManager(layoutManager1)
        addonAdapter = AddonAdapter(addOnModels, this@BookingDetailActivity, R.layout.row_addon)
        insp_rl = findViewById(R.id.insp_rl)
        remarks_rl = findViewById(R.id.remarks_rl)
        services_rl = findViewById(R.id.services_rl)
        addon_rl = findViewById(R.id.addon_rl)
        photo_rl = findViewById(R.id.photo_rl)
        loc_rl = findViewById(R.id.loc_rl)
        loc_rl?.setOnClickListener(this)
        photos_recyclerview = findViewById(R.id.photos_recyclerview)
        photosAdapter = PhotosAdapter(mylist, this@BookingDetailActivity, R.layout.row_photos)
        photos_recyclerview?.setLayoutManager(
            GridLayoutManager(
                this,
                1,
                GridLayoutManager.HORIZONTAL,
                false
            )
        )
        address_txt = findViewById(R.id.address_txt)
        address_txt?.setTypeface(bold)
        pick_date_txt = findViewById(R.id.pick_date_txt)
        pick_date_txt?.setTypeface(regular)
        pick_time_txt = findViewById(R.id.pick_time_txt)
        pick_time_txt?.setTypeface(regular)
        pickup_txt = findViewById(R.id.pickup_txt)
        pickup_txt?.setTypeface(regular)
        pickup_txt?.setOnClickListener(this)
        discount_tr = findViewById(R.id.discount_tr)
        addon_tr = findViewById(R.id.addon_tr)
        est_txt = findViewById(R.id.est_txt)
        est_txt?.setTypeface(bold)
        est_price_txt = findViewById(R.id.est_price_txt)
        est_price_txt?.setTypeface(bold)
        init_txt = findViewById(R.id.init_txt)
        init_txt?.setTypeface(bold)
        init_price_txt = findViewById(R.id.init_price_txt)
        init_price_txt?.setTypeface(bold)
        disc_txt = findViewById(R.id.disc_txt)
        disc_txt?.setTypeface(bold)
        dicount_txt = findViewById(R.id.dicount_txt)
        dicount_txt?.setTypeface(bold)
        insp_txt = findViewById(R.id.insp_txt)
        insp_txt?.setTypeface(bold)
        addon_txt = findViewById(R.id.addon_txt)
        addon_txt?.setTypeface(bold)
        addon_price_txt = findViewById(R.id.addon_price_txt)
        addon_price_txt?.setTypeface(bold)
        tot_txt = findViewById(R.id.tot_txt)
        tot_txt?.setTypeface(bold)
        final_price_txt = findViewById(R.id.final_price_txt)
        final_price_txt?.setTypeface(bold)
        photos_txt = findViewById(R.id.photos_txt)
        photos_txt?.setTypeface(bold)
        payment_status_txt = findViewById(R.id.payment_status_txt)
        payment_status_txt?.setTypeface(bold)
        moving_txt = findViewById(R.id.moving_txt)
        moving_txt?.setTypeface(bold)
        kilometers_txt = findViewById(R.id.kilometers_txt)
        kilometers_txt?.setTypeface(regular)
        historyDetail
    }

    //SimpleDateFormat timeFormatter = new SimpleDateFormat("h a");
    private val historyDetail: Unit
        private get() {
            val url: String =
                AppUrls.BASE_URL + AppUrls.HISTORY_DETAIL.toString() + bookingId.toString() + "/detail"
            Log.d("URL", url)
            val stringRequest: StringRequest = object : StringRequest(
                Method.GET,
                url,
                Response.Listener { response ->
                    Log.d("RESP", response)
                    try {
                        val jsonObject = JSONObject(response)
                        val bookingId = jsonObject.optString("bookingId")
                        booking_txt!!.text = "Booking Id : $bookingId"
                        userId = jsonObject.optString("userId")
                        val orderId = jsonObject.optString("orderId")
                        serviceCenterId = jsonObject.optString("serviceCenterId")
                        val bookingType = jsonObject.optString("bookingType")
                        brandId = jsonObject.optString("brandId")
                        brandName = jsonObject.optString("brandName")
                        modelId = jsonObject.optString("modelId")
                        modelName = jsonObject.optString("modelName")
                        val kilometerRangeId =
                            jsonObject.optString("kilometerRangeId")
                        val bookingDate = jsonObject.optString("bookingDate")
                        val bookingTime = jsonObject.optString("bookingTime")
                        val pickupAddress = jsonObject.optString("pickupAddress")
                        pickupAddressLatitude = jsonObject.optString("pickupAddressLatitude")
                        pickupAddressLongitude = jsonObject.optString("pickupAddressLongitude")
                        val pickedUpTime = jsonObject.optString("pickedUpTime")
                        val deliveredTime = jsonObject.optString("deliveredTime")
                        val addressType = jsonObject.optString("addressType")
                        val estimatedCost = jsonObject.optString("estimatedCost")
                        val finalPrice = jsonObject.optString("finalPrice")
                        val inspectionWords = jsonObject.optString("inspectionWords")
                        val promocodeAmount = jsonObject.optString("promocodeAmount")
                        val initialPaidAmount =
                            jsonObject.optString("initialPaidAmount")
                        val paymentStatus = jsonObject.optString("paymentStatus")
                        val movingCondition =
                            jsonObject.optBoolean("movingCondition")
                        if (movingCondition) {
                            moving_txt!!.setTextColor(
                                ContextCompat.getColor(
                                    this@BookingDetailActivity,
                                    R.color.green
                                )
                            )
                            moving_txt!!.text = "Vehicle Is In Moving Condition"
                        } else {
                            moving_txt!!.setTextColor(
                                ContextCompat.getColor(
                                    this@BookingDetailActivity,
                                    R.color.red
                                )
                            )
                            moving_txt!!.text = "Vehicle Is Not In Moving Condition"
                        }
                        if (paymentStatus.equals("2", ignoreCase = true)) {
                            payment_status_txt!!.text = "Final Payment Done"
                        } else {
                            payment_status_txt!!.setTextColor(Color.RED)
                            payment_status_txt!!.text = "Final Payment Pending"
                        }
                        status = jsonObject.optString("status")
                        val deleted = jsonObject.optString("deleted")
                        val createdTime = jsonObject.optString("createdTime")
                        val modifiedTime = jsonObject.optString("modifiedTime")
                        val firstName = jsonObject.optString("firstName")
                        val lastName = jsonObject.optString("lastName")
                        val serviceCenterName =
                            jsonObject.optString("serviceCenterName")
                        sst_name_txt!!.text = serviceCenterName
                        sstMobileNumber = jsonObject.optString("contactMobileNumber")
                        mobile = jsonObject.optString("mobile")
                        val rescheduleCount = jsonObject.optString("rescheduleCount")
                        val paidAmount = jsonObject.optString("paidAmount")
                        val addonsAmount = jsonObject.optString("addonsAmount")
                        val registrationNumber =
                            jsonObject.optString("registrationNumber")
                        reg_txt!!.text = registrationNumber
                        customer_mobile_txt!!.text = mobile
                        sst_mobile_txt!!.text = sstMobileNumber
                        if (inspectionWords.isEmpty()) {
                            remarks_rl!!.visibility = View.GONE
                        } else {
                            remarks_rl!!.visibility = View.VISIBLE
                            remark_txt!!.text = inspectionWords
                        }
                        if (jsonObject.has("fileIds")) {
                            photos_txt!!.visibility = View.VISIBLE
                            photo_rl!!.visibility = View.VISIBLE
                            photos_recyclerview!!.visibility = View.VISIBLE
                            val jsonArray = jsonObject.getJSONArray("fileIds")
                            for (i in 0 until jsonArray.length()) {
                                val data = jsonArray[i].toString()
                                mylist.add(AppUrls.IMAGE_URL.toString() + data)
                            }
                            Log.d("PHOTOS", mylist.toString())
                            photos_recyclerview!!.adapter = photosAdapter
                            photosAdapter?.notifyDataSetChanged()
                        } else {
                            photos_txt!!.visibility = View.GONE
                            photo_rl!!.visibility = View.GONE
                            photos_recyclerview!!.visibility = View.GONE
                        }
                        if (jsonObject.has("services")) {
                            val jsonArray = jsonObject.getJSONArray("services")
                            if (jsonArray.length() > 0) {
                                for (i in 0 until jsonArray.length()) {
                                    val jsonObject1 = jsonArray.getJSONObject(i)
                                    val servicesModel = BookingServicesModel()
                                    servicesModel.serviceId=jsonObject1.optString("serviceId")
                                    servicesModel.cost=jsonObject1.optString("cost")
                                    servicesModel.serviceName=jsonObject1.optString("serviceName")
                                    servicesModel.status=jsonObject1.optString("status")
                                    servicesModels.add(servicesModel)
                                }
                                services_recyclerview!!.adapter = servicesAdapter
                                servicesAdapter?.notifyDataSetChanged()
                            } else {
                                services_rl!!.visibility = View.GONE
                            }
                        }
                        if (jsonObject.has("inspections")) {
                            val jsonArray = jsonObject.getJSONArray("inspections")
                            if (jsonArray.length() > 0) {
                                insp_rl!!.visibility = View.VISIBLE
                                for (i in 0 until jsonArray.length()) {
                                    val jsonObject1 = jsonArray.getJSONObject(i)
                                    val inspectionModel = InspectionModel()
                                    inspectionModel.inspectionId=jsonObject1.optString("inspectionId")
                                    inspectionModel.bookingId=jsonObject1.optString("bookingId")
                                    inspectionModel.state=jsonObject1.optString("state")
                                    inspectionModel.inspectionName=jsonObject1.optString("inspectionName")
                                    inspectionModels.add(inspectionModel)
                                }
                                insp_recyclerview!!.adapter = inspAdapter
                                inspAdapter?.notifyDataSetChanged()
                            } else {
                                insp_rl!!.visibility = View.GONE
                            }
                        }
                        if (jsonObject.has("bookingAddonServices")) {
                            val jsonArray =
                                jsonObject.getJSONArray("bookingAddonServices")
                            if (jsonArray.length() > 0) {
                                for (i in 0 until jsonArray.length()) {
                                    val jsonObject1 = jsonArray.getJSONObject(i)
                                    val servicesModel = BookingServicesModel()
                                    servicesModel.serviceId=jsonObject1.optString("serviceId")
                                    servicesModel.cost=jsonObject1.optString("cost")
                                    servicesModel.serviceName=jsonObject1.optString("serviceName")
                                    servicesModel.status=jsonObject1.optString("status")
                                    addOnModels.add(servicesModel)
                                }
                                addon_recyclerview!!.adapter = addonAdapter
                                addonAdapter?.notifyDataSetChanged()
                            } else {
                                addon_rl!!.visibility = View.GONE
                            }
                        }
                        if (jsonObject.has("kilometerRange")) {
                            val jsonObject1 = jsonObject.getJSONObject("kilometerRange")
                            startRange = jsonObject1.optString("startRange")
                            endRange = jsonObject1.optString("endRange")
                            kilometers_txt!!.text = "$startRange - $endRange km"
                        }
                        if (pickedUpTime.isEmpty()) {
                            pick_date_txt!!.visibility = View.GONE
                        } else {
                            pick_date_txt!!.visibility = View.VISIBLE
                            pick_date_txt!!.text = "Pick Up : $pickedUpTime"
                        }
                        if (deliveredTime.isEmpty()) {
                            pick_time_txt!!.visibility = View.GONE
                        } else {
                            pick_time_txt!!.visibility = View.VISIBLE
                            pick_time_txt!!.text = "Delivery : $deliveredTime"
                        }
                        val resultDate = convertStringDateToAnotherStringDate(
                            bookingDate,
                            "yyyy-MM-dd",
                            "dd-MM-yyyy"
                        )
                        Log.d("ResultDate", resultDate)
                        val df = SimpleDateFormat("HH:mm:ss")
                        var d: Date? = null
                        try {
                            d = df.parse(bookingTime)
                        } catch (e: ParseException) {
                            e.printStackTrace()
                        }
                        val cal = Calendar.getInstance()
                        cal.time = d
                        cal.add(Calendar.HOUR, 5)
                        cal.add(Calendar.MINUTE, 30)
                        var newTime = df.format(cal.time)
                        try {
                            val dateFormatter =
                                SimpleDateFormat("HH:mm:ss")
                            val date = dateFormatter.parse(newTime)
                            val timeFormatter =
                                SimpleDateFormat("h:mm a")
                            //SimpleDateFormat timeFormatter = new SimpleDateFormat("h a");
                            newTime = timeFormatter.format(date)
                        } catch (e: ParseException) {
                            e.printStackTrace()
                        }
                        brand_txt!!.text = brandName
                        model_txt!!.text = modelName
                        date_txt!!.text = resultDate
                        time_txt!!.text = newTime
                        pickup_txt!!.text = pickupAddress
                        if (addonsAmount.equals("0", ignoreCase = true)) {
                            addon_tr!!.visibility = View.GONE
                        } else {
                            addon_tr!!.visibility = View.VISIBLE
                            addon_price_txt!!.text = "\u20B9 $addonsAmount"
                        }
                        if (promocodeAmount.isEmpty() || promocodeAmount.equals(
                                "0",
                                ignoreCase = true
                            )
                        ) {
                            discount_tr!!.visibility = View.GONE
                        } else {
                            discount_tr!!.visibility = View.VISIBLE
                            dicount_txt!!.text = "\u20B9 $promocodeAmount"
                        }
                        est_price_txt!!.text = "\u20B9 $estimatedCost"
                        init_price_txt!!.text = "\u20B9 $initialPaidAmount"
                        final_price_txt!!.text = "\u20B9 $paidAmount"
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                },
                Response.ErrorListener { error: VolleyError? ->
                    if (error is TimeoutError || error is NoConnectionError) {
                    } else if (error is AuthFailureError) {
                    } else if (error is ServerError) {
                    } else if (error is NetworkError) {
                    } else if (error is ParseError) {
                    }
                }
            ) {
                override fun getHeaders(): Map<String, String> {
                    val headers: MutableMap<String, String> =
                        HashMap()
                    headers["Authorization"] = "Bearer $accessToken"
                    return headers
                }
            }
            val requestQueue = Volley.newRequestQueue(this@BookingDetailActivity)
            requestQueue.add(stringRequest)
        }

    fun convertStringDateToAnotherStringDate(
        stringdate: String,
        stringdateformat: String?,
        returndateformat: String?
    ): String {
        return try {
            val date =
                SimpleDateFormat(stringdateformat).parse(stringdate)
            SimpleDateFormat(returndateformat).format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
            stringdate
        }
    }

    override fun onClick(v: View) {
        if (v === close) {
            if (checkInternet) {
                if (activity.equals("NewHistoryAdapter", ignoreCase = true)) {
                    val intent =
                        Intent(this@BookingDetailActivity, TodayHistoryActivity::class.java)
                    startActivity(intent)
                } else if (activity.equals("HistoryAdapter", ignoreCase = true)) {
                    val intent = Intent(
                        this@BookingDetailActivity,
                        UpcomingHistoryActivity::class.java
                    )
                    startActivity(intent)
                } else if (activity.equals("DeliveredHistoryAdapter", ignoreCase = true)) {
                    val intent =
                        Intent(this@BookingDetailActivity, DeliveredActivity::class.java)
                    startActivity(intent)
                } else if (activity.equals("NotificationAdapter", ignoreCase = true)) {
                    val intent =
                        Intent(this@BookingDetailActivity, NotificationActivity::class.java)
                    startActivity(intent)
                } else if (activity.equals("ConcernAdapter", ignoreCase = true)) {
                    val intent =
                        Intent(this@BookingDetailActivity, ConcernListActivity::class.java)
                    startActivity(intent)
                } else {
                    val intent =
                        Intent(this@BookingDetailActivity, MainActivity::class.java)
                    startActivity(intent)
                }
            } else {
                val snackbar = Snackbar.make(
                    window.decorView,
                    "Check Internet Connection",
                    Snackbar.LENGTH_LONG
                )
                snackbar.show()
            }
        }
        if (v === customer_mobile_txt) {
            if (checkInternet) {
                val callIntent = Intent(Intent.ACTION_DIAL)
                callIntent.data = Uri.parse("tel:" + Uri.encode(mobile))
                callIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(callIntent)
            } else {
                val snackbar = Snackbar.make(
                    window.decorView,
                    "Check Internet Connection",
                    Snackbar.LENGTH_LONG
                )
                snackbar.show()
            }
        }
        if (v === sst_mobile_txt) {
            if (checkInternet) {
                val callIntent = Intent(Intent.ACTION_DIAL)
                callIntent.data = Uri.parse(
                    "tel:" + Uri.encode(
                        sstMobileNumber
                    )
                )
                callIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(callIntent)
            } else {
                val snackbar = Snackbar.make(
                    window.decorView,
                    "Check Internet Connection",
                    Snackbar.LENGTH_LONG
                )
                snackbar.show()
            }
        }
        if (v === loc_rl || v === pickup_txt || v === redirect_img) {
            if (checkInternet) {
                val uri =
                    "http://maps.google.com/maps?q=loc:$pickupAddressLatitude,$pickupAddressLongitude (Customer)"
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
                startActivity(intent)
            } else {
                val snackbar = Snackbar.make(
                    window.decorView,
                    "Check Internet Connection",
                    Snackbar.LENGTH_LONG
                )
                snackbar.show()
            }
        }
    }

    override fun onBackPressed() {
        if (activity.equals("NewHistoryAdapter", ignoreCase = true)) {
            val intent =
                Intent(this@BookingDetailActivity, TodayHistoryActivity::class.java)
            startActivity(intent)
        } else if (activity.equals("HistoryAdapter", ignoreCase = true)) {
            val intent =
                Intent(this@BookingDetailActivity, UpcomingHistoryActivity::class.java)
            startActivity(intent)
        } else if (activity.equals("DeliveredHistoryAdapter", ignoreCase = true)) {
            val intent = Intent(this@BookingDetailActivity, DeliveredActivity::class.java)
            startActivity(intent)
        } else if (activity.equals("NotificationAdapter", ignoreCase = true)) {
            val intent =
                Intent(this@BookingDetailActivity, NotificationActivity::class.java)
            startActivity(intent)
        } else if (activity.equals("ConcernAdapter", ignoreCase = true)) {
            val intent = Intent(this@BookingDetailActivity, ConcernListActivity::class.java)
            startActivity(intent)
        } else {
            val intent = Intent(this@BookingDetailActivity, MainActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onRefresh() {
        swipe!!.isRefreshing = false
        val intent = Intent(this@BookingDetailActivity, BookingDetailActivity::class.java)
        intent.putExtra("activity", "BookingDetailActivity")
        intent.putExtra("bookingId", bookingId)
        startActivity(intent)
    }
}
