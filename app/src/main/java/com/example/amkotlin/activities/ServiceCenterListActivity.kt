package com.example.amkotlin.activities

import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.*
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.amkotlin.MainActivity
import com.example.amkotlin.R
import com.example.amkotlin.adapters.ServiceCenterListAdapter
import com.example.amkotlin.models.ServiceCenterListModel
import com.example.amkotlin.utils.AppUrls
import com.example.amkotlin.utils.GlobalCalls
import com.example.amkotlin.utils.NetworkChecking
import com.example.amkotlin.utils.UserSessionManager
import com.google.android.material.snackbar.Snackbar
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class ServiceCenterListActivity : AppCompatActivity(),
    View.OnClickListener {
    private var checkInternet = false
    var close: ImageView? = null
    var toolbar_title: TextView? = null
    var regular: Typeface? = null
    var bold: Typeface? = null
    var userSessionManager: UserSessionManager? = null
    var sst_search: SearchView? = null
    var sst_recyclerview: RecyclerView? = null
    var serviceCenterListAdapter: ServiceCenterListAdapter? = null
    var serviceCenterListModels: ArrayList<ServiceCenterListModel> =
        ArrayList<ServiceCenterListModel>()
    var accessToken: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_service_center_list)
        checkInternet = NetworkChecking.isConnected(this)
        regular = Typeface.createFromAsset(assets, "proxima_nova_regular.otf")
        bold = Typeface.createFromAsset(assets, "proxima_nova_bold.otf")
        userSessionManager = UserSessionManager(this)
        val userDetails: HashMap<String, String?>? = userSessionManager?.userDetails
        accessToken = userDetails?.get(UserSessionManager.KEY_ACCSES)
        close = findViewById(R.id.close)
        close?.setOnClickListener(this)
        toolbar_title = findViewById(R.id.toolbar_title)
        toolbar_title?.setTypeface(bold)
        sst_recyclerview = findViewById(R.id.sst_recyclerview)
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(applicationContext)
        sst_recyclerview?.setLayoutManager(layoutManager)
        serviceCenterListAdapter = ServiceCenterListAdapter(
            serviceCenterListModels,
            this@ServiceCenterListActivity,
            R.layout.row_sst
        )
        sstList
        sst_search = findViewById(R.id.sst_search)
        val searchEditText =
            sst_search?.findViewById<EditText>(androidx.appcompat.R.id.search_src_text)
        sst_search?.setOnClickListener(View.OnClickListener { v: View? ->
            sst_search?.setIconified(
                false
            )
        })
        sst_search?.setOnQueryTextListener(object :
            SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

            override fun onQueryTextChange(query: String): Boolean {
                serviceCenterListAdapter?.getFilter()?.filter(query)
                return false
            }
        })
    }

    private val sstList: Unit
        private get() {
            val url: String = AppUrls.BASE_URL + AppUrls.SST_LIST
            val stringRequest: StringRequest =
                object : StringRequest(
                    Method.GET,
                    url,
                    Response.Listener { response ->
                        serviceCenterListModels.clear()
                        try {
                            val jsonObject = JSONObject(response)
                            val status = jsonObject.getString("status")
                            val message = jsonObject.getString("message")
                            if (status.equals("2000", ignoreCase = true)) {
                                val jsonObject1 = jsonObject.optJSONObject("data")
                                val jsonArray =
                                    jsonObject1.optJSONArray("serviceCenters")
                                if (jsonArray.length() != 0) {
                                    for (i in 0 until jsonArray.length()) {
                                        val jsonObject2 = jsonArray.optJSONObject(i)
                                        val sclm = ServiceCenterListModel()
                                        sclm.serviceCenterId=(jsonObject2.optString("serviceCenterId"))
                                        sclm.serviceCenterName=(jsonObject2.optString("serviceCenterName"))
                                        sclm.contactName=(jsonObject2.optString("contactName"))
                                        sclm.userId=(jsonObject2.optString("userId"))
                                        sclm.contactMobileNumber=(jsonObject2.optString("contactMobileNumber"))
                                        sclm.contactEmail=(jsonObject2.optString("contactEmail"))
                                        sclm.address=(jsonObject2.optString("address"))
                                        sclm.latitude=(jsonObject2.optString("latitude"))
                                        sclm.longitude=(jsonObject2.optString("longitude"))
                                        sclm.openTime=(jsonObject2.optString("openTime"))
                                        sclm.closeTime=(jsonObject2.optString("closeTime"))
                                        sclm.averageRating=(jsonObject2.optString("averageRating"))
                                        sclm.deleted=(jsonObject2.optString("deleted"))
                                        sclm.createdTime=(jsonObject2.optString("createdTime"))
                                        sclm.modifiedTime=(jsonObject2.optString("modifiedTime"))
                                        sclm.displayTime=(jsonObject2.optString("displayTime"))
                                        sclm.areaManagerId=(jsonObject2.optString("areaManagerId"))
                                        sclm.areaManagerName=(jsonObject2.optString("areaManagerName"))
                                        sclm.areaManagerNumber=(jsonObject2.optString("areaManagerNumber"))
                                        serviceCenterListModels.add(sclm)
                                    }
                                    sst_recyclerview!!.adapter = serviceCenterListAdapter
                                    serviceCenterListAdapter?.notifyDataSetChanged()
                                } else {
                                    GlobalCalls.showToast(
                                        "No Data Available",
                                        this@ServiceCenterListActivity
                                    )
                                }
                            }
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    },
                    Response.ErrorListener { error ->
                        if (error is TimeoutError || error is NoConnectionError) {
                        } else if (error is AuthFailureError) {
                        } else if (error is ServerError) {
                        } else if (error is NetworkError) {
                        } else if (error is ParseError) {
                        }
                    }
                ) {
                    override fun getHeaders(): Map<String, String> {
                        val headers: MutableMap<String, String> =
                            HashMap()
                        headers["Authorization"] = "Bearer $accessToken"
                        return headers
                    }
                }
            val requestQueue = Volley.newRequestQueue(this@ServiceCenterListActivity)
            requestQueue.add(stringRequest)
        }

    override fun onClick(v: View) {
        if (v === close) {
            if (checkInternet) {
                val intent =
                    Intent(this@ServiceCenterListActivity, MainActivity::class.java)
                startActivity(intent)
            } else {
                val snackbar = Snackbar.make(
                    window.decorView,
                    "Check Internet Connection",
                    Snackbar.LENGTH_LONG
                )
                snackbar.show()
            }
        }
    }
}

