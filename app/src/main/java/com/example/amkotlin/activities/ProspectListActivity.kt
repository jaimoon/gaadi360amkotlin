package com.example.amkotlin.activities

import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.*
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.amkotlin.MainActivity
import com.example.amkotlin.R
import com.example.amkotlin.adapters.ProspectListAdapter
import com.example.amkotlin.models.ProspectListModel
import com.example.amkotlin.utils.AppUrls
import com.example.amkotlin.utils.GlobalCalls
import com.example.amkotlin.utils.NetworkChecking
import com.example.amkotlin.utils.UserSessionManager
import com.google.android.material.snackbar.Snackbar
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class ProspectListActivity : AppCompatActivity(), View.OnClickListener {
    private var checkInternet = false
    var close: ImageView? = null
    var add_img: ImageView? = null
    var toolbar_title: TextView? = null
    var regular: Typeface? = null
    var bold: Typeface? = null
    var userSessionManager: UserSessionManager? = null
    var prospect_search: SearchView? = null
    var prospect_recyclerview: RecyclerView? = null
    var prospectListAdapter: ProspectListAdapter? = null
    var prospectListModels: ArrayList<ProspectListModel> = ArrayList<ProspectListModel>()
    var accessToken: String? = null
    var dialogBuilder: AlertDialog.Builder? = null
    var alertDialog: AlertDialog? = null
    var mobile_edt: EditText? = null
    var first_name_edt: EditText? = null
    var last_name_edt: EditText? = null
    var email_edt: EditText? = null
    var message_edt: EditText? = null
    var date_txt: TextView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_prospect_list)
        checkInternet = NetworkChecking.isConnected(this)
        regular = Typeface.createFromAsset(assets, "proxima_nova_regular.otf")
        bold = Typeface.createFromAsset(assets, "proxima_nova_bold.otf")
        userSessionManager = UserSessionManager(this)
        val userDetails: HashMap<String, String?>? = userSessionManager?.userDetails
        accessToken = userDetails?.get(UserSessionManager.KEY_ACCSES)
        close = findViewById(R.id.close)
        close?.setOnClickListener(this)
        add_img = findViewById(R.id.add_img)
        add_img?.setOnClickListener(this)
        toolbar_title = findViewById(R.id.toolbar_title)
        toolbar_title?.setTypeface(bold)
        prospect_recyclerview = findViewById(R.id.prospect_recyclerview)
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(applicationContext)
        prospect_recyclerview?.setLayoutManager(layoutManager)
        prospectListAdapter = ProspectListAdapter(
            prospectListModels,
            this@ProspectListActivity,
            R.layout.row_prospects
        )
        prospectList
        prospect_search = findViewById(R.id.prospect_search)
        val searchEditText =
            prospect_search?.findViewById<EditText>(androidx.appcompat.R.id.search_src_text)
        prospect_search?.setOnClickListener(View.OnClickListener { v: View? ->
            prospect_search?.setIconified(
                false
            )
        })
        prospect_search?.setOnQueryTextListener(object :
            SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

            override fun onQueryTextChange(query: String): Boolean {

                //serviceCenterListAdapter.getFilter().filter(query);
                return false
            }
        })
    }

    private val prospectList: Unit
        private get() {
            val url: String = AppUrls.BASE_URL + AppUrls.PROSPECTS_LIST
            val stringRequest: StringRequest =
                object : StringRequest(
                    Method.GET,
                    url,
                    Response.Listener { response ->
                        prospectListModels.clear()
                        try {
                            val jsonObject = JSONObject(response)
                            val status = jsonObject.getString("status")
                            val message = jsonObject.getString("message")
                            if (status.equals("2000", ignoreCase = true)) {
                                val jsonObject1 = jsonObject.optJSONObject("data")
                                val jsonArray = jsonObject1.optJSONArray("prospects")
                                if (jsonArray.length() != 0) {
                                    for (i in 0 until jsonArray.length()) {
                                        val jsonObject2 = jsonArray.optJSONObject(i)
                                        val plm = ProspectListModel()
                                        plm.id=(jsonObject2.optString("id"))
                                        plm.username=(jsonObject2.optString("username"))
                                        plm.firstName=(jsonObject2.optString("firstName"))
                                        plm.lastName=(jsonObject2.optString("lastName"))
                                        plm.bookingDate=(jsonObject2.optString("bookingDate"))
                                        plm.comments=(jsonObject2.optString("comments"))
                                        plm.email=(jsonObject2.optString("email"))
                                        plm.createdTime=(jsonObject2.optString("createdTime"))
                                        plm.areaManagerName=(jsonObject2.optString("areaManagerName"))
                                        plm.areaManagerNumber=(jsonObject2.optString("areaManagerNumber"))
                                        plm.type=(jsonObject2.optString("type"))
                                        prospectListModels.add(plm)
                                    }
                                    prospect_recyclerview!!.adapter = prospectListAdapter
                                    prospectListAdapter?.notifyDataSetChanged()
                                } else {
                                    GlobalCalls.showToast(
                                        "No Data Available",
                                        this@ProspectListActivity
                                    )
                                }
                            }
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    },
                    Response.ErrorListener { error ->
                        if (error is TimeoutError || error is NoConnectionError) {
                        } else if (error is AuthFailureError) {
                        } else if (error is ServerError) {
                        } else if (error is NetworkError) {
                        } else if (error is ParseError) {
                        }
                    }
                ) {
                    override fun getHeaders(): Map<String, String> {
                        val headers: MutableMap<String, String> =
                            HashMap()
                        headers["Authorization"] = "Bearer $accessToken"
                        return headers
                    }
                }
            val requestQueue = Volley.newRequestQueue(this@ProspectListActivity)
            requestQueue.add(stringRequest)
        }

    override fun onClick(v: View) {
        if (v === close) {
            if (checkInternet) {
                val intent = Intent(this@ProspectListActivity, MainActivity::class.java)
                startActivity(intent)
            } else {
                val snackbar = Snackbar.make(
                    window.decorView,
                    "Check Internet Connection",
                    Snackbar.LENGTH_LONG
                )
                snackbar.show()
            }
        }
        if (v === add_img) {
            if (checkInternet) {
                showProspectDialog(R.layout.prospect_dialog)
            } else {
                val snackbar = Snackbar.make(
                    window.decorView,
                    "Check Internet Connection",
                    Snackbar.LENGTH_LONG
                )
                snackbar.show()
            }
        }
    }

    private fun showProspectDialog(layout: Int) {
        dialogBuilder = AlertDialog.Builder(this@ProspectListActivity)
        val layoutView = layoutInflater.inflate(layout, null)
        val prospect_title = layoutView.findViewById<TextView>(R.id.prospect_title)
        prospect_title.setTypeface(bold)
        mobile_edt = layoutView.findViewById(R.id.mobile_edt)
        mobile_edt?.setTypeface(regular)
        first_name_edt = layoutView.findViewById(R.id.first_name_edt)
        first_name_edt?.setTypeface(regular)
        last_name_edt = layoutView.findViewById(R.id.last_name_edt)
        last_name_edt?.setTypeface(regular)
        email_edt = layoutView.findViewById(R.id.email_edt)
        email_edt?.setTypeface(regular)
        date_txt = layoutView.findViewById(R.id.date_txt)
        date_txt?.setTypeface(regular)
        message_edt = layoutView.findViewById(R.id.message_edt)
        message_edt?.setTypeface(regular)
        date_txt?.setOnClickListener(View.OnClickListener { date })
        val cancel_btn =
            layoutView.findViewById<Button>(R.id.cancel_btn)
        cancel_btn.setTypeface(bold)
        val add_btn =
            layoutView.findViewById<Button>(R.id.add_btn)
        add_btn.setTypeface(bold)
        cancel_btn.setOnClickListener { alertDialog!!.dismiss() }
        add_btn.setOnClickListener {
            if (mobile_edt?.getText().toString().isEmpty()) {
                mobile_edt?.setError("Please Enter Mobile Number")
            } else if (first_name_edt?.getText().toString().isEmpty()) {
                first_name_edt?.setError("Please Enter First Name")
            }
            if (date_txt?.getText().toString()
                    .equals("Select Booking Date", ignoreCase = true)
            ) {
                date_txt?.setError("Please Select Date")
            } else {
                addProspects()
            }
        }
        dialogBuilder!!.setView(layoutView)
        alertDialog = dialogBuilder!!.create()
        alertDialog!!.window!!.attributes.windowAnimations = R.style.DialogAnimation
        alertDialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        alertDialog!!.show()
        alertDialog!!.setCancelable(false)
    }

    val date: Unit
        get() {
            val calendar = Calendar.getInstance()
            val day = calendar[Calendar.DAY_OF_MONTH]
            val month = calendar[Calendar.MONTH]
            val year = calendar[Calendar.YEAR]
            val datePickerDialog = DatePickerDialog(
                this,
                OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    val strDate =
                        dayOfMonth.toString() + "-" + (monthOfYear + 1) + "-" + year
                    date_txt!!.text = strDate
                }, year, month, day
            )
            datePickerDialog.setButton(
                DialogInterface.BUTTON_NEGATIVE,
                "Cancel"
            ) { dialog, which -> }
            datePickerDialog.datePicker.minDate = System.currentTimeMillis()
            datePickerDialog.show()
        }

    private fun addProspects() {
        val url: String = AppUrls.BASE_URL + AppUrls.ADD_PROSPECT
        val jsonObj = JSONObject()
        try {
            jsonObj.put("username", mobile_edt!!.text.toString().trim { it <= ' ' })
            jsonObj.put("firstName", first_name_edt!!.text.toString())
            jsonObj.put("lastName", last_name_edt!!.text.toString())
            jsonObj.put("bookingDate", date_txt!!.text.toString())
            jsonObj.put("comments", message_edt!!.text.toString())
            jsonObj.put("email", email_edt!!.text.toString())
        } catch (e: Exception) {
        }
        Log.d("JsonObj", jsonObj.toString())
        val request: JsonObjectRequest = object : JsonObjectRequest(
            Method.POST,
            url,
            jsonObj,
            Response.Listener { response ->
                Log.d("ProspectListActivity", response.toString())
                try {
                    val jsonObject = JSONObject(response.toString())
                    val status = jsonObject.getString("status")
                    val message = jsonObject.getString("message")
                    Log.d(
                        "Status", """
     $status
     $message
     """.trimIndent()
                    )
                    if (status.equals("2000", ignoreCase = true)) {
                        alertDialog!!.dismiss()
                        GlobalCalls.showToast(message, this@ProspectListActivity)
                        prospectList
                    }
                    if (status.equals("5001", ignoreCase = true)) {
                        GlobalCalls.showToast(message, this@ProspectListActivity)
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                    Log.d("ResError", e.message)
                }
            },
            Response.ErrorListener { error ->
                error.printStackTrace()
                Log.d("EROORRR", error.message)
            }) {
            override fun getHeaders(): Map<String, String> {
                val headers: MutableMap<String, String> =
                    HashMap()
                headers["Authorization"] = "Bearer $accessToken"
                return headers
            }
        }
        val requestQueue = Volley.newRequestQueue(this@ProspectListActivity)
        requestQueue.add(request)
    }
}

