package com.example.amkotlin.activities

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.example.amkotlin.MainActivity
import com.example.amkotlin.R
import com.example.amkotlin.utils.UserSessionManager
import java.util.*

class SplashScreenActivity : AppCompatActivity() {

    var session: UserSessionManager? = null
    var accessToken: String? = null
    var device_id: kotlin.String? = null

    @SuppressLint("HardwareIds")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        session = UserSessionManager(applicationContext)
        val userDetails: HashMap<String, String?>? = session?.userDetails
        accessToken = userDetails?.get(UserSessionManager.KEY_ACCSES)

        device_id = Settings.Secure.getString(
            applicationContext.contentResolver,
            Settings.Secure.ANDROID_ID
        )
        session!!.createDeviceId(device_id)

        Log.d("Splash", accessToken + "\n" + device_id)

        val SPLASH_TIME_OUT = 1850

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Handler().postDelayed({
                if (session!!.checkLogin() !== false) {
                    val intent =
                        Intent(this@SplashScreenActivity, LoginActivity::class.java)
                    startActivity(intent)
                    finish()
                } else {
                    val intent =
                        Intent(this@SplashScreenActivity, MainActivity::class.java)
                    startActivity(intent)
                    finish()
                }
            }, SPLASH_TIME_OUT.toLong())
        } else {
            Handler().postDelayed({
                if (session!!.checkLogin() !== false) {
                    val intent =
                        Intent(this@SplashScreenActivity, LoginActivity::class.java)
                    startActivity(intent)
                    finish()
                } else {
                    val intent =
                        Intent(this@SplashScreenActivity, MainActivity::class.java)
                    startActivity(intent)
                    finish()
                }
            }, SPLASH_TIME_OUT.toLong())
        }
    }
}
