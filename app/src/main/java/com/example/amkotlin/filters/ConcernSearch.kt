package com.example.amkotlin.filters

import android.widget.Filter
import com.example.amkotlin.adapters.ConcernAdapter
import com.example.amkotlin.models.ConcernModel
import java.util.*

class ConcernSearch(
    var filterList: ArrayList<ConcernModel>,
    var adapter: ConcernAdapter
) : Filter() {
    override fun performFiltering(constraint: CharSequence): FilterResults {
        var constraint: CharSequence? = constraint
        val results = FilterResults()
        if (constraint != null && constraint.length > 0) {
            constraint = constraint.toString().toUpperCase()
            val filteredPlayers =
                ArrayList<ConcernModel>()
            for (i in filterList.indices) {
                if (filterList[i].serviceCenterName!!.toUpperCase().contains(constraint) ||
                    filterList[i].concernId!!.toUpperCase().contains(constraint) ||
                    filterList[i].bookingId!!.toUpperCase().contains(constraint) ||
                    filterList[i].concernMessage!!.toUpperCase().contains(constraint) ||
                    filterList[i].bookingDate!!.toUpperCase().contains(constraint) ||
                    filterList[i].bookingTime!!.toUpperCase().contains(constraint) ||
                    filterList[i].deliveredTime!!.toUpperCase().contains(constraint) ||
                    filterList[i].concernTime!!.toUpperCase().contains(constraint) ||
                    filterList[i].userMobile!!.toUpperCase().contains(constraint) ||
                    filterList[i].userName!!.toUpperCase().contains(constraint) ||
                    filterList[i].finalPrice!!.toUpperCase().contains(constraint)
                ) {
                    filteredPlayers.add(filterList[i])
                }
            }
            results.count = filteredPlayers.size
            results.values = filteredPlayers
        } else {
            results.count = filterList.size
            results.values = filterList
        }
        return results
    }

    override fun publishResults(
        constraint: CharSequence,
        results: FilterResults
    ) {
        adapter.concernModels = results.values as ArrayList<ConcernModel>
        adapter.notifyDataSetChanged()
    }

}