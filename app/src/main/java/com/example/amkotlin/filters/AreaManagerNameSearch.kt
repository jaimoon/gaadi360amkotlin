package com.example.amkotlin.filters

import android.widget.Filter
import com.example.amkotlin.adapters.AreaManagerNameAdapter
import com.example.amkotlin.models.TrackingModel

class AreaManagerNameSearch(
    var filterList: ArrayList<TrackingModel>,
    var adapter: AreaManagerNameAdapter
) : Filter() {
    override fun performFiltering(constraint: CharSequence): FilterResults {
        var constraint: CharSequence? = constraint
        val results = FilterResults()
        if (constraint != null && constraint.length > 0) {
            constraint = constraint.toString().toUpperCase()
            val filteredPlayers =
                ArrayList<TrackingModel>()
            for (i in filterList.indices) {
                if (filterList[i].userName!!.toUpperCase().contains(constraint)) {
                    filteredPlayers.add(filterList[i])
                }
            }
            results.count = filteredPlayers.size
            results.values = filteredPlayers
        } else {
            results.count = filterList.size
            results.values = filterList
        }
        return results
    }

    override fun publishResults(
        constraint: CharSequence,
        results: FilterResults
    ) {
        adapter.trackingModels = results.values as java.util.ArrayList<TrackingModel>
        adapter.notifyDataSetChanged()
    }

}