package com.example.amkotlin.filters

import android.widget.Filter
import com.example.amkotlin.adapters.TrackingAdapter
import com.example.amkotlin.models.TrackingModel
import java.util.*

class TrackingSearch(var filterList: ArrayList<TrackingModel>, var adapter: TrackingAdapter) : Filter() {
    override fun performFiltering(constraint: CharSequence): FilterResults {
        var constraint: CharSequence? = constraint
        val results = FilterResults()
        if (constraint != null && constraint.length > 0) {
            constraint = constraint.toString().toUpperCase()
            val filteredPlayers =
                ArrayList<TrackingModel>()
            for (i in filterList.indices) {
                if (filterList[i].id!!.toUpperCase().contains(constraint) ||
                    filterList[i].userId!!.toUpperCase().contains(constraint) ||
                    filterList[i].userName!!.toUpperCase().contains(constraint) ||
                    filterList[i].getCreatedTime()!!.toUpperCase().contains(constraint) ||
                    filterList[i].latitude!!.toUpperCase().contains(constraint) ||
                    filterList[i].longitude!!.toUpperCase().contains(constraint) ||
                    filterList[i].location!!.toUpperCase().contains(constraint)
                ) {
                    filteredPlayers.add(filterList[i])
                }
            }
            results.count = filteredPlayers.size
            results.values = filteredPlayers
        } else {
            results.count = filterList.size
            results.values = filterList
        }
        return results
    }

    override fun publishResults(
        constraint: CharSequence,
        results: FilterResults
    ) {
        adapter.trackingModels = results.values as ArrayList<TrackingModel>
        adapter.notifyDataSetChanged()
    }

}