package com.example.amkotlin.filters

import android.widget.Filter
import com.example.amkotlin.adapters.ServiceCenterListAdapter
import com.example.amkotlin.models.ServiceCenterListModel
import java.util.*

class ServiceCenterListSearch(
    var filterList: ArrayList<ServiceCenterListModel>,
    var adapter: ServiceCenterListAdapter
) : Filter() {
    override fun performFiltering(constraint: CharSequence): FilterResults {
        var constraint: CharSequence? = constraint
        val results = FilterResults()
        if (constraint != null && constraint.length > 0) {
            constraint = constraint.toString().toUpperCase()
            val filteredPlayers =
                ArrayList<ServiceCenterListModel>()
            for (i in filterList.indices) {
                if (filterList[i].serviceCenterId!!.toUpperCase().contains(constraint) ||
                    filterList[i].serviceCenterName!!.toUpperCase()
                        .contains(constraint) ||
                    filterList[i].contactName!!.toUpperCase().contains(constraint) ||
                    filterList[i].userId!!.toUpperCase().contains(constraint) ||
                    filterList[i].contactMobileNumber!!.toUpperCase()
                        .contains(constraint) ||
                    filterList[i].contactEmail!!.toUpperCase().contains(constraint) ||
                    filterList[i].address!!.toUpperCase().contains(constraint) ||
                    filterList[i].latitude!!.toUpperCase().contains(constraint) ||
                    filterList[i].longitude!!.toUpperCase().contains(constraint) ||
                    filterList[i].openTime!!.toUpperCase().contains(constraint) ||
                    filterList[i].closeTime!!.toUpperCase().contains(constraint) ||
                    filterList[i].averageRating!!.toUpperCase().contains(constraint) ||
                    filterList[i].displayTime!!.toUpperCase().contains(constraint) ||
                    filterList[i].areaManagerName!!.toUpperCase().contains(constraint) ||
                    filterList[i].areaManagerNumber!!.toUpperCase()
                        .contains(constraint) ||
                    filterList[i].areaManagerId!!.toUpperCase().contains(constraint)
                ) {
                    filteredPlayers.add(filterList[i])
                }
            }
            results.count = filteredPlayers.size
            results.values = filteredPlayers
        } else {
            results.count = filterList.size
            results.values = filterList
        }
        return results
    }

    override fun publishResults(
        constraint: CharSequence,
        results: FilterResults
    ) {
        adapter.serviceCenterListModels = results.values as ArrayList<ServiceCenterListModel>
        adapter.notifyDataSetChanged()
    }

}