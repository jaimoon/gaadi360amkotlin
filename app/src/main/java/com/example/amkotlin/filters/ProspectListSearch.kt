package com.example.amkotlin.filters

import android.widget.Filter
import com.example.amkotlin.adapters.ProspectListAdapter
import com.example.amkotlin.models.ProspectListModel
import java.util.*

class ProspectListSearch(
    var filterList: ArrayList<ProspectListModel>,
    var adapter: ProspectListAdapter
) : Filter() {
    override fun performFiltering(constraint: CharSequence): FilterResults {
        var constraint: CharSequence? = constraint
        val results = FilterResults()
        if (constraint != null && constraint.length > 0) {
            constraint = constraint.toString().toUpperCase()
            val filteredPlayers =
                ArrayList<ProspectListModel>()
            for (i in filterList.indices) {
                if (filterList[i].id!!.toUpperCase().contains(constraint) ||
                    filterList[i].username!!.toUpperCase().contains(constraint) ||
                    filterList[i].firstName!!.toUpperCase().contains(constraint) ||
                    filterList[i].lastName!!.toUpperCase().contains(constraint) ||
                    filterList[i].bookingDate!!.toUpperCase().contains(constraint) ||
                    filterList[i].comments!!.toUpperCase().contains(constraint) ||
                    filterList[i].createdTime!!.toUpperCase().contains(constraint) ||
                    filterList[i].email!!.toUpperCase().contains(constraint) ||
                    filterList[i].areaManagerName!!.toUpperCase().contains(constraint) ||
                    filterList[i].areaManagerNumber!!.toUpperCase()
                        .contains(constraint) ||
                    filterList[i].type!!.toUpperCase().contains(constraint)
                ) {
                    filteredPlayers.add(filterList[i])
                }
            }
            results.count = filteredPlayers.size
            results.values = filteredPlayers
        } else {
            results.count = filterList.size
            results.values = filterList
        }
        return results
    }

    override fun publishResults(
        constraint: CharSequence,
        results: FilterResults
    ) {
        adapter.prospectListModels = results.values as ArrayList<ProspectListModel>
        adapter.notifyDataSetChanged()
    }

}