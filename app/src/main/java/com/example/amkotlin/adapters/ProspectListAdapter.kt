package com.example.amkotlin.adapters

import android.content.Context
import android.content.Intent
import android.graphics.Paint
import android.graphics.Typeface
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.amkotlin.R
import com.example.amkotlin.activities.ProspectListActivity
import com.example.amkotlin.adapters.ProspectListAdapter.ProspectListHolder
import com.example.amkotlin.filters.ProspectListSearch
import com.example.amkotlin.interfaces.ProspectListItemClickListener
import com.example.amkotlin.models.ProspectListModel
import com.example.amkotlin.utils.DateTimeUtils.formatDate
import java.util.*

class ProspectListAdapter(
    var prospectListModels: ArrayList<ProspectListModel>,
    context: ProspectListActivity,
    resource: Int
) : RecyclerView.Adapter<ProspectListHolder>(), Filterable {
    var filterList: ArrayList<ProspectListModel>
    var context: ProspectListActivity
    var filter: ProspectListSearch? = null
    private val li: LayoutInflater
    private val resource: Int
    private var lastPosition = -1
    var regular: Typeface? = null
    var bold: Typeface? = null
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ProspectListHolder {
        val view =
            LayoutInflater.from(context).inflate(R.layout.row_prospects, viewGroup, false)
        return ProspectListHolder(view)
    }

    override fun onBindViewHolder(
        prospectListHolder: ProspectListHolder,
        position: Int
    ) {
        setAnimation(prospectListHolder.itemView, position)
        prospectListHolder.bind(prospectListModels[position])
    }

    override fun getItemCount(): Int {
        return prospectListModels.size
    }

    override fun getFilter(): Filter {
        if (filter == null) {
            filter = ProspectListSearch(filterList, this)
        }
        return filter!!
    }

    inner class ProspectListHolder internal constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView), View.OnClickListener {
        var call_img: ImageView
        var email_img: ImageView
        var am_call_img: ImageView
        var name_txt: TextView
        var booking_date_txt: TextView
        var mobile_txt: TextView
        var email_txt: TextView
        var comments_txt: TextView
        var am_name_txt: TextView
        var am_mobile_txt: TextView
        var prospectListItemClickListener: ProspectListItemClickListener? = null
        fun bind(prospectListModel: ProspectListModel) {
            regular = Typeface.createFromAsset(context.assets, "proxima_nova_regular.otf")
            bold = Typeface.createFromAsset(context.assets, "proxima_nova_bold.otf")
            name_txt.typeface = bold
            booking_date_txt.typeface = bold
            comments_txt.typeface = regular
            am_name_txt.typeface = regular
            mobile_txt.typeface = regular
            mobile_txt.paintFlags = mobile_txt.paintFlags or Paint.UNDERLINE_TEXT_FLAG
            email_txt.typeface = regular
            email_txt.paintFlags = email_txt.paintFlags or Paint.UNDERLINE_TEXT_FLAG
            am_mobile_txt.typeface = regular
            am_mobile_txt.paintFlags = am_mobile_txt.paintFlags or Paint.UNDERLINE_TEXT_FLAG


            /*if (!prospectListModel.getFirstName().equalsIgnoreCase("null") || !prospectListModel.getLastName().equalsIgnoreCase("null")) {
                name_txt.setText(prospectListModel.getFirstName() + " " + prospectListModel.getLastName());
            } else {
                name_txt.setText("No name");
            }*/name_txt.text = prospectListModel.firstName + " " + prospectListModel.lastName
            if (!prospectListModel.bookingDate.equals("null", ignoreCase = true)) {
                val resultDate = formatDate(
                    prospectListModel.bookingDate!!,
                    "yyyy-MM-dd",
                    "dd-MM-yyyy"
                )
                booking_date_txt.text = "Schedule Booking - $resultDate"
            } else {
                booking_date_txt.text = "No Date"
            }
            if (!prospectListModel.comments.equals("null", ignoreCase = true)) {
                comments_txt.text = prospectListModel.comments
            } else {
                comments_txt.text = "Not available"
            }
            if (!prospectListModel.username!!.isEmpty()) {
                call_img.visibility = View.VISIBLE
                mobile_txt.visibility = View.VISIBLE
                mobile_txt.text = prospectListModel.username
            } else {
                call_img.visibility = View.GONE
                mobile_txt.visibility = View.GONE
            }
            if (!prospectListModel.email.equals("null", ignoreCase = true)) {
                email_img.visibility = View.VISIBLE
                email_txt.visibility = View.VISIBLE
                email_txt.text = prospectListModel.email
            } else {
                email_img.visibility = View.GONE
                email_txt.visibility = View.GONE
            }
            if (!prospectListModel.areaManagerName.equals("", ignoreCase = true)) {
                am_name_txt.visibility = View.VISIBLE
                am_name_txt.text = prospectListModel.areaManagerName + "  (Area Manager)"
            } else {
                am_name_txt.visibility = View.GONE
                am_name_txt.text = "No name"
            }
            if (!prospectListModel.areaManagerNumber!!.isEmpty()) {
                am_call_img.visibility = View.VISIBLE
                am_mobile_txt.visibility = View.VISIBLE
                am_mobile_txt.text = prospectListModel.areaManagerNumber
            } else {
                am_call_img.visibility = View.GONE
                am_mobile_txt.visibility = View.GONE
            }
            mobile_txt.setOnClickListener {
                val callIntent = Intent(Intent.ACTION_DIAL)
                callIntent.data = Uri.parse(
                    "tel:" + Uri.encode(
                        prospectListModel.username
                    )
                )
                callIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                context.startActivity(callIntent)
            }
            email_txt.setOnClickListener {
                val emailIntent = Intent(Intent.ACTION_SEND)
                emailIntent.type = "plain/text"
                emailIntent.putExtra(
                    Intent.EXTRA_EMAIL,
                    arrayOf(prospectListModel.email)
                )
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject")
                emailIntent.putExtra(Intent.EXTRA_TEXT, "Text")
                context.startActivity(Intent.createChooser(emailIntent, "Send mail..."))
            }
            am_mobile_txt.setOnClickListener {
                val callIntent = Intent(Intent.ACTION_DIAL)
                callIntent.data = Uri.parse(
                    "tel:" + Uri.encode(
                        prospectListModel.areaManagerNumber
                    )
                )
                callIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                context.startActivity(callIntent)
            }
            itemView.setOnClickListener { view: View? -> }
        }

        override fun onClick(view: View) {
            prospectListItemClickListener!!.onItemClick(view, layoutPosition)
        }

        init {
            itemView.setOnClickListener(this)
            call_img = itemView.findViewById(R.id.call_img)
            email_img = itemView.findViewById(R.id.email_img)
            am_call_img = itemView.findViewById(R.id.am_call_img)
            name_txt = itemView.findViewById(R.id.name_txt)
            booking_date_txt = itemView.findViewById(R.id.booking_date_txt)
            mobile_txt = itemView.findViewById(R.id.mobile_txt)
            email_txt = itemView.findViewById(R.id.email_txt)
            comments_txt = itemView.findViewById(R.id.comments_txt)
            am_name_txt = itemView.findViewById(R.id.am_name_txt)
            am_mobile_txt = itemView.findViewById(R.id.am_mobile_txt)
        }
    }

    private fun setAnimation(viewToAnimate: View, position: Int) {
        if (position > lastPosition) {
            val animation = AnimationUtils.loadAnimation(
                context,
                android.R.anim.slide_in_left
            )
            viewToAnimate.startAnimation(animation)
            lastPosition = position
        }
    }

    init {
        filterList = prospectListModels
        this.context = context
        this.resource = resource
        li =
            context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    }
}