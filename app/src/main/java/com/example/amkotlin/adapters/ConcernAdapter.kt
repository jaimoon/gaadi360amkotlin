package com.example.amkotlin.adapters

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.example.amkotlin.R
import com.example.amkotlin.activities.BookingDetailActivity
import com.example.amkotlin.activities.ConcernListActivity
import com.example.amkotlin.adapters.ConcernAdapter.ConcernHolder
import com.example.amkotlin.filters.ConcernSearch
import com.example.amkotlin.interfaces.ConcernItemClickListener
import com.example.amkotlin.models.ConcernModel
import com.example.amkotlin.utils.AppUrls
import com.example.amkotlin.utils.GlobalCalls.showToast
import com.example.amkotlin.utils.UserSessionManager
import org.json.JSONException
import org.json.JSONObject
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class ConcernAdapter(
    var concernModels: ArrayList<ConcernModel>,
    context: Activity,
    resource: Int
) : RecyclerView.Adapter<ConcernHolder>(), Filterable {
    var filterList: ArrayList<ConcernModel>
    var context: Activity
    var filter: ConcernSearch? = null
    private val li: LayoutInflater
    private val resource: Int
    private var lastPosition = -1
    var regular: Typeface? = null
    var bold: Typeface? = null
    var dialogBuilder: AlertDialog.Builder? = null
    var alertDialog: AlertDialog? = null
    var concern_edt: EditText? = null
    var accessToken: String? = null
    var userSessionManager: UserSessionManager? = null
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ConcernHolder {
        val view =
            LayoutInflater.from(context).inflate(R.layout.row_concern, viewGroup, false)
        return ConcernHolder(view)
    }

    override fun onBindViewHolder(concernHolder: ConcernHolder, position: Int) {
        setAnimation(concernHolder.itemView, position)
        concernHolder.bind(concernModels[position])
        userSessionManager = UserSessionManager(context)
        val userDetails =
            userSessionManager!!.userDetails
        accessToken = userDetails[UserSessionManager.KEY_ACCSES]
    }

    override fun getItemCount(): Int {
        return concernModels.size
    }

    override fun getFilter(): Filter {
        if (filter == null) {
            filter = ConcernSearch(filterList, this)
        }
        return filter!!
    }

    inner class ConcernHolder internal constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView), View.OnClickListener {
        var concernItemClickListener: ConcernItemClickListener? = null
        var booking_id_txt: TextView
        var sst_txt: TextView
        var desp_txt: TextView
        var res_txt: TextView
        var user_number_title_txt: TextView
        var user_number_txt: TextView
        var time_txt: TextView
        var desp_title: TextView
        var res_title: TextView
        var resolve_btn: Button
        var concernId: String? = null
        var bookingId: String? = null
        var sstName: String? = null
        var desp: String? = null
        var resolveMessage: String? = null
        var userMobile: String? = null
        var bookingDate: String? = null
        var bookingTime: String? = null
        var status: String? = null
        fun bind(concernModel: ConcernModel) {
            regular = Typeface.createFromAsset(context.assets, "proxima_nova_regular.otf")
            bold = Typeface.createFromAsset(context.assets, "proxima_nova_bold.otf")
            booking_id_txt.typeface = bold
            sst_txt.typeface = bold
            desp_txt.typeface = regular
            res_txt.typeface = regular
            user_number_title_txt.typeface = bold
            user_number_txt.typeface = bold
            time_txt.typeface = regular
            desp_title.typeface = bold
            res_title.typeface = bold
            concernId = concernModel.concernId
            bookingId = concernModel.bookingId
            sstName = concernModel.serviceCenterName
            desp = concernModel.concernMessage
            resolveMessage = concernModel.resolvedMessage
            userMobile = concernModel.userMobile
            bookingDate = concernModel.bookingDate
            bookingTime = concernModel.bookingTime
            status = concernModel.status
            if (status.equals("1", ignoreCase = true)) {
                resolve_btn.text = "Resolve"
                resolve_btn.setBackgroundResource(R.drawable.orange_rounded_button)
                resolve_btn.setOnClickListener {
                    showConcernAlertDialog(
                        R.layout.dialog_concern_layout,
                        concernId
                    )
                }
            } else {
                resolve_btn.text = "Resolved"
                resolve_btn.setBackgroundResource(R.drawable.green_rounded_button)
            }
            booking_id_txt.text = "Booking Id : $bookingId"
            sst_txt.text = sstName
            desp_txt.text = desp
            if (!resolveMessage.equals("null", ignoreCase = true)) {
                res_title.visibility = View.VISIBLE
                res_txt.text = resolveMessage
            } else {
                res_title.visibility = View.GONE
                res_txt.visibility = View.GONE
            }
            user_number_txt.paintFlags = user_number_txt.paintFlags or Paint.UNDERLINE_TEXT_FLAG
            user_number_txt.text = userMobile
            val resultDate = convertStringDateToAnotherStringDate(
                concernModel.bookingDate,
                "yyyy-MM-dd",
                "dd-MM-yyyy"
            )
            Log.d("ResultDate", resultDate)
            val df = SimpleDateFormat("HH:mm:ss")
            var d: Date? = null
            try {
                d = df.parse(bookingTime)
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            val cal = Calendar.getInstance()
            cal.time = d
            cal.add(Calendar.HOUR, 5)
            cal.add(Calendar.MINUTE, 30)
            var newTime = df.format(cal.time)
            try {
                val dateFormatter =
                    SimpleDateFormat("HH:mm:ss")
                val date = dateFormatter.parse(newTime)
                val timeFormatter = SimpleDateFormat("h:mm a")
                //SimpleDateFormat timeFormatter = new SimpleDateFormat("h a");
                newTime = timeFormatter.format(date)
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            time_txt.text = "Booking Time $resultDate $newTime"
            user_number_title_txt.setOnClickListener {
                val callIntent = Intent(Intent.ACTION_DIAL)
                callIntent.data = Uri.parse(
                    "tel:" + Uri.encode(
                        userMobile
                    )
                )
                callIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                context.startActivity(callIntent)
            }
            user_number_txt.setOnClickListener {
                val callIntent = Intent(Intent.ACTION_DIAL)
                callIntent.data = Uri.parse(
                    "tel:" + Uri.encode(
                        userMobile
                    )
                )
                callIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                context.startActivity(callIntent)
            }
            itemView.setOnClickListener { view: View? ->
                val intent = Intent(context, BookingDetailActivity::class.java)
                intent.putExtra("activity", "ConcernAdapter")
                intent.putExtra("bookingId", bookingId)
                context.startActivity(intent)
            }
        }

        override fun onClick(view: View) {
            concernItemClickListener!!.onItemClick(view, layoutPosition)
        }

        init {
            itemView.setOnClickListener(this)
            booking_id_txt = itemView.findViewById(R.id.booking_id_txt)
            sst_txt = itemView.findViewById(R.id.sst_txt)
            desp_txt = itemView.findViewById(R.id.desp_txt)
            res_txt = itemView.findViewById(R.id.res_txt)
            user_number_title_txt = itemView.findViewById(R.id.user_number_title_txt)
            user_number_txt = itemView.findViewById(R.id.user_number_txt)
            time_txt = itemView.findViewById(R.id.time_txt)
            resolve_btn = itemView.findViewById(R.id.resolve_btn)
            desp_title = itemView.findViewById(R.id.desp_title)
            res_title = itemView.findViewById(R.id.res_title)
        }
    }

    private fun showConcernAlertDialog(layout: Int, concernId: String?) {
        dialogBuilder = AlertDialog.Builder(context)
        val layoutView = context.layoutInflater.inflate(layout, null)
        concern_edt = layoutView.findViewById(R.id.concern_edt)
        val cancel_btn =
            layoutView.findViewById<Button>(R.id.cancel_btn)
        val submit_btn =
            layoutView.findViewById<Button>(R.id.submit_btn)
        dialogBuilder!!.setView(layoutView)
        alertDialog = dialogBuilder!!.create()
        alertDialog!!.window!!.attributes.windowAnimations = R.style.DialogAnimation
        alertDialog!!.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        alertDialog!!.show()
        cancel_btn.setOnClickListener { alertDialog!!.dismiss() }
        submit_btn.setOnClickListener {
            sendConcern(concernId)
        }
        alertDialog!!.setCancelable(false)
    }

    private fun sendConcern(concernId: String?) {
        val url = AppUrls.BASE_URL + AppUrls.RESOLVE_CONCERN
        val jsonObj = JSONObject()
        try {
            jsonObj.put("concernId", concernId)
            jsonObj.put("resolvedMessage", concern_edt!!.text.toString())
        } catch (e: Exception) {
        }
        Log.d("JsonObj", jsonObj.toString())
        val request: JsonObjectRequest = object : JsonObjectRequest(
            Method.PUT,
            url,
            jsonObj,
            Response.Listener { response ->
                Log.d("ConcernStatus", response.toString())
                try {
                    val jsonObject = JSONObject(response.toString())
                    val status = jsonObject.getString("status")
                    val message = jsonObject.getString("message")
                    Log.d(
                        "Status", """
     $status
     $message
     """.trimIndent()
                    )
                    if (status.equals("2000", ignoreCase = true)) {
                        alertDialog!!.dismiss()
                        showToast("Your Concern Submitted Successfully..!", context)
                        val intent = Intent(context, ConcernListActivity::class.java)
                        context.startActivity(intent)
                    } else {
                        showToast("Something went wrong..!", context)
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                    Log.d("ResError", e.message)
                }
            },
            Response.ErrorListener { error ->
                error.printStackTrace()
                Log.d("EROORRR", error.message)
            }) {
            override fun getHeaders(): Map<String, String> {
                val headers: MutableMap<String, String> =
                    HashMap()
                headers["Authorization"] = "Bearer $accessToken"
                return headers
            }
        }
        val requestQueue = Volley.newRequestQueue(context)
        requestQueue.add(request)
    }

    fun convertStringDateToAnotherStringDate(
        stringdate: String?,
        stringdateformat: String?,
        returndateformat: String?
    ): String? {
        return try {
            val date =
                SimpleDateFormat(stringdateformat).parse(stringdate)
            SimpleDateFormat(returndateformat).format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
            stringdate
        }
    }

    private fun setAnimation(viewToAnimate: View, position: Int) {
        if (position > lastPosition) {
            val animation = AnimationUtils.loadAnimation(
                context,
                android.R.anim.slide_in_left
            )
            viewToAnimate.startAnimation(animation)
            lastPosition = position
        }
    }

    init {
        filterList = concernModels
        this.context = context
        this.resource = resource
        li =
            context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    }
}