package com.example.amkotlin.adapters

import android.content.Context
import android.content.Intent
import android.graphics.Paint
import android.graphics.Typeface
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.amkotlin.R
import com.example.amkotlin.activities.TrackingActivity
import com.example.amkotlin.filters.TrackingSearch
import com.example.amkotlin.interfaces.TrackingItemClickListener
import com.example.amkotlin.models.TrackingModel
import java.util.*

class TrackingAdapter(var trackingModels: ArrayList<TrackingModel>, context: TrackingActivity, resource: Int) :
    RecyclerView.Adapter<TrackingAdapter.TrackingHolder>(), Filterable {
    var filterList: ArrayList<TrackingModel>
    var context: TrackingActivity
    var filter: TrackingSearch? = null
    private val li: LayoutInflater
    private val resource: Int
    private var lastPosition = -1
    var regular: Typeface? = null
    var bold: Typeface? = null
    override fun onCreateViewHolder(
        viewGroup: ViewGroup,
        i: Int
    ): TrackingHolder {
        val view =
            LayoutInflater.from(context).inflate(R.layout.row_tracking, viewGroup, false)
        return TrackingHolder(view)
    }

    override fun onBindViewHolder(
        historyHolder: TrackingHolder,
        position: Int
    ) {
        setAnimation(historyHolder.itemView, position)
        historyHolder.bind(trackingModels[position])
    }

    override fun getItemCount(): Int {
        return trackingModels.size
    }

    override fun getFilter(): Filter {
        if (filter == null) {
            filter = TrackingSearch(filterList, this)
        }
        return filter as TrackingSearch
    }

    inner class TrackingHolder internal constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView), View.OnClickListener {
        var redirect_img: ImageView
        var name_txt: TextView
        var mobile_txt: TextView
        var location_txt: TextView
        var time_txt: TextView
        var trackingItemClickListener: TrackingItemClickListener? = null
        fun bind(trackingModel: TrackingModel) {
            regular = Typeface.createFromAsset(context.assets, "proxima_nova_regular.otf")
            bold = Typeface.createFromAsset(context.assets, "proxima_nova_bold.otf")
            name_txt.typeface = bold
            location_txt.typeface = regular
            time_txt.typeface = regular
            mobile_txt.typeface = regular
            mobile_txt.paintFlags = mobile_txt.paintFlags or Paint.UNDERLINE_TEXT_FLAG
            if (!trackingModel.userName.equals("null", ignoreCase = true)) {
                name_txt.text = trackingModel.userName
            } else {
                name_txt.text = "No name"
            }
            if (!trackingModel.location.equals("null", ignoreCase = true)) {
                location_txt.text = trackingModel.location
            } else {
                location_txt.text = "Location unavailable"
            }
            if (!trackingModel.getCreatedTime().equals("null", ignoreCase = true)) {
                time_txt.text = trackingModel.getCreatedTime()
            } else {
                time_txt.text = "Not found"
            }
            if (!trackingModel.mobile.equals("null", ignoreCase = true)) {
                mobile_txt.text = trackingModel.mobile
            } else {
                mobile_txt.text = "Not found"
            }
            mobile_txt.setOnClickListener {
                val callIntent = Intent(Intent.ACTION_DIAL)
                callIntent.data = Uri.parse(
                    "tel:" + Uri.encode(
                        trackingModel.mobile
                    )
                )
                callIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                context.startActivity(callIntent)
            }
            redirect_img.setOnClickListener {
                val uri =
                    "http://maps.google.com/maps?q=loc:" + trackingModel.latitude + "," + trackingModel.longitude + " (" + "Area Managet" + ")"
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
                context.startActivity(intent)
            }
            itemView.setOnClickListener { view: View? -> }
        }

        override fun onClick(view: View) {
            trackingItemClickListener!!.onItemClick(view, layoutPosition)
        }

        init {
            itemView.setOnClickListener(this)
            redirect_img = itemView.findViewById(R.id.redirect_img)
            name_txt = itemView.findViewById(R.id.name_txt)
            location_txt = itemView.findViewById(R.id.location_txt)
            time_txt = itemView.findViewById(R.id.time_txt)
            mobile_txt = itemView.findViewById(R.id.mobile_txt)
        }
    }

    private fun setAnimation(viewToAnimate: View, position: Int) {
        if (position > lastPosition) {
            val animation = AnimationUtils.loadAnimation(
                context,
                android.R.anim.slide_in_left
            )
            viewToAnimate.startAnimation(animation)
            lastPosition = position
        }
    }

    init {
        filterList = trackingModels
        this.context = context
        this.resource = resource
        li =
            context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    }
}