package com.example.amkotlin.adapters

import android.content.Context
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.amkotlin.R
import com.example.amkotlin.activities.BookingDetailActivity
import com.example.amkotlin.interfaces.AddonHistoryItemClickListener
import com.example.amkotlin.models.BookingServicesModel

class AddonAdapter(
    var servicesModels: List<BookingServicesModel>,
    var context: BookingDetailActivity,
    private val resource: Int
) : RecyclerView.Adapter<AddonAdapter.AddonHolder>() {
    private val li: LayoutInflater
    private var lastPosition = -1
    var regular: Typeface? = null
    var bold: Typeface? = null
    override fun onCreateViewHolder(
        viewGroup: ViewGroup,
        i: Int
    ): AddonHolder {
        val view =
            LayoutInflater.from(context).inflate(R.layout.row_addon, viewGroup, false)
        return AddonHolder(view)
    }

    override fun onBindViewHolder(
        historyHolder: AddonHolder,
        position: Int
    ) {
        setAnimation(historyHolder.itemView, position)
        historyHolder.bind(servicesModels[position])
    }

    override fun getItemCount(): Int {
        return servicesModels.size
    }

    inner class AddonHolder internal constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView), View.OnClickListener {
        var service_txt: TextView
        var cost_txt: TextView
        var status_txt: TextView
        var addonHistoryItemClickListener: AddonHistoryItemClickListener? = null
        fun bind(servicesModel: BookingServicesModel) {
            regular = Typeface.createFromAsset(context.assets, "proxima_nova_regular.otf")
            bold = Typeface.createFromAsset(context.assets, "proxima_nova_bold.otf")
            val service = servicesModel.serviceName
            val cost = servicesModel.cost
            val status = servicesModel.status
            service_txt.typeface = regular
            cost_txt.typeface = regular
            status_txt.typeface = bold
            service_txt.text = service
            cost_txt.text = "\u20B9 $cost"
            if (status.equals("0", ignoreCase = true)) {
                status_txt.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary))
                status_txt.text = "Pending"
            }
            if (status.equals("1", ignoreCase = true)) {
                status_txt.setTextColor(ContextCompat.getColor(context, R.color.green))
                status_txt.text = "Approved"
            }
            if (status.equals("2", ignoreCase = true)) {
                status_txt.setTextColor(ContextCompat.getColor(context, R.color.red))
                status_txt.text = "Rejected"
            }
            itemView.setOnClickListener { view: View? -> }
        }

        override fun onClick(view: View) {
            addonHistoryItemClickListener!!.onItemClick(view, layoutPosition)
        }

        init {
            itemView.setOnClickListener(this)
            service_txt = itemView.findViewById(R.id.service_txt)
            cost_txt = itemView.findViewById(R.id.cost_txt)
            status_txt = itemView.findViewById(R.id.status_txt)
        }
    }

    private fun setAnimation(viewToAnimate: View, position: Int) {
        if (position > lastPosition) {
            val animation = AnimationUtils.loadAnimation(
                context,
                android.R.anim.slide_in_left
            )
            viewToAnimate.startAnimation(animation)
            lastPosition = position
        }
    }

    init {
        li =
            context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    }
}