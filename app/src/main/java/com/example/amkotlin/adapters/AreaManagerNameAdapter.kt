package com.example.amkotlin.adapters

import android.content.Context
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.amkotlin.R
import com.example.amkotlin.activities.TrackingActivity
import com.example.amkotlin.filters.AreaManagerNameSearch
import com.example.amkotlin.interfaces.TrackingItemClickListener
import com.example.amkotlin.models.TrackingModel
import java.util.*

class AreaManagerNameAdapter(
    var trackingModels: ArrayList<TrackingModel>,
    context: TrackingActivity,
    resource: Int
) : RecyclerView.Adapter<AreaManagerNameAdapter.TrackingHolder>(),
    Filterable {
    var filterList: ArrayList<TrackingModel>
    var context: TrackingActivity
    var filter: AreaManagerNameSearch? = null
    private val li: LayoutInflater
    private val resource: Int
    private var lastPosition = -1
    var regular: Typeface? = null
    var bold: Typeface? = null
    override fun onCreateViewHolder(
        viewGroup: ViewGroup,
        i: Int
    ): TrackingHolder {
        val view =
            LayoutInflater.from(context).inflate(R.layout.row_names, viewGroup, false)
        return TrackingHolder(view)
    }

    override fun onBindViewHolder(
        historyHolder: TrackingHolder,
        position: Int
    ) {
        setAnimation(historyHolder.itemView, position)
        historyHolder.bind(trackingModels[position])
    }

    override fun getItemCount(): Int {
        return trackingModels.size
    }

    override fun getFilter(): Filter {
        if (filter == null) {
            filter = AreaManagerNameSearch(filterList, this)
        }
        return filter as AreaManagerNameSearch
    }

    inner class TrackingHolder internal constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView), View.OnClickListener {
        var area_manager_name_txt: TextView
        var trackingItemClickListener: TrackingItemClickListener? = null
        fun bind(trackingModel: TrackingModel) {
            regular = Typeface.createFromAsset(context.assets, "proxima_nova_regular.otf")
            bold = Typeface.createFromAsset(context.assets, "proxima_nova_bold.otf")
            area_manager_name_txt.typeface = bold
            if (!trackingModel.userName.equals("null", ignoreCase = true)) {
                area_manager_name_txt.text = trackingModel.userName
            } else {
                area_manager_name_txt.text = "No name"
            }
            itemView.setOnClickListener { view: View? ->
                context.setAreaManagerName(
                    trackingModel.userId,
                    trackingModel.userName,
                    trackingModel.mobile
                )
            }
        }

        override fun onClick(view: View) {
            trackingItemClickListener!!.onItemClick(view, layoutPosition)
        }

        init {
            itemView.setOnClickListener(this)
            area_manager_name_txt = itemView.findViewById(R.id.area_manager_name_txt)
        }
    }

    private fun setAnimation(viewToAnimate: View, position: Int) {
        if (position > lastPosition) {
            val animation = AnimationUtils.loadAnimation(
                context,
                android.R.anim.slide_in_left
            )
            viewToAnimate.startAnimation(animation)
            lastPosition = position
        }
    }

    init {
        filterList = trackingModels
        this.context = context
        this.resource = resource
        li =
            context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    }
}