package com.example.amkotlin.adapters

import android.content.Context
import android.content.Intent
import android.graphics.Paint
import android.graphics.Typeface
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.example.amkotlin.R
import com.example.amkotlin.activities.ServiceCenterListActivity
import com.example.amkotlin.adapters.ServiceCenterListAdapter.ServiceCenterListHolder
import com.example.amkotlin.filters.ServiceCenterListSearch
import com.example.amkotlin.interfaces.SstItemClickListener
import com.example.amkotlin.models.ServiceCenterListModel
import java.util.*

class ServiceCenterListAdapter(
    var serviceCenterListModels: ArrayList<ServiceCenterListModel>,
    context: ServiceCenterListActivity,
    resource: Int
) : RecyclerView.Adapter<ServiceCenterListHolder>(), Filterable {
    var filterList: ArrayList<ServiceCenterListModel>
    var context: ServiceCenterListActivity
    var filter: ServiceCenterListSearch? = null
    private val li: LayoutInflater
    private val resource: Int
    private var lastPosition = -1
    var regular: Typeface? = null
    var bold: Typeface? = null
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ServiceCenterListHolder {
        val view =
            LayoutInflater.from(context).inflate(R.layout.row_sst, viewGroup, false)
        return ServiceCenterListHolder(view)
    }

    override fun onBindViewHolder(
        serviceCenterListHolder: ServiceCenterListHolder,
        position: Int
    ) {
        setAnimation(serviceCenterListHolder.itemView, position)
        serviceCenterListHolder.bind(serviceCenterListModels[position])
    }

    override fun getItemCount(): Int {
        return serviceCenterListModels.size
    }

    override fun getFilter(): Filter {
        if (filter == null) {
            filter = ServiceCenterListSearch(filterList, this)
        }
        return filter!!
    }

    inner class ServiceCenterListHolder internal constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView), View.OnClickListener {
        var redirect_img: ImageView
        var am_call_img: ImageView
        var sst_name_txt: TextView
        var name_txt: TextView
        var mobile_txt: TextView
        var email_txt: TextView
        var location_txt: TextView
        var time_txt: TextView
        var am_name_txt: TextView
        var am_mobile_txt: TextView
        var sst_rating: RatingBar
        var sstItemClickListener: SstItemClickListener? = null
        fun bind(serviceCenterListModel: ServiceCenterListModel) {
            regular = Typeface.createFromAsset(context.assets, "proxima_nova_regular.otf")
            bold = Typeface.createFromAsset(context.assets, "proxima_nova_bold.otf")
            am_name_txt.typeface = bold
            sst_name_txt.typeface = bold
            name_txt.typeface = bold
            location_txt.typeface = regular
            time_txt.typeface = regular
            mobile_txt.typeface = regular
            mobile_txt.paintFlags = mobile_txt.paintFlags or Paint.UNDERLINE_TEXT_FLAG
            email_txt.typeface = regular
            email_txt.paintFlags = email_txt.paintFlags or Paint.UNDERLINE_TEXT_FLAG
            am_mobile_txt.typeface = regular
            am_mobile_txt.paintFlags = am_mobile_txt.paintFlags or Paint.UNDERLINE_TEXT_FLAG
            if (!serviceCenterListModel.serviceCenterName.equals("null", ignoreCase = true)) {
                sst_name_txt.text = serviceCenterListModel.serviceCenterName
            } else {
                sst_name_txt.text = "No name"
            }
            if (!serviceCenterListModel.contactName.equals("null", ignoreCase = true)) {
                name_txt.text = serviceCenterListModel.contactName
            } else {
                name_txt.text = "No name"
            }
            if (!serviceCenterListModel.address.equals("null", ignoreCase = true)) {
                location_txt.text = serviceCenterListModel.address
            } else {
                location_txt.text = "Location unavailable"
            }
            if (!serviceCenterListModel.displayTime.equals("null", ignoreCase = true)) {
                time_txt.text = serviceCenterListModel.displayTime
            } else {
                time_txt.text = "Not found"
            }
            if (!serviceCenterListModel.contactMobileNumber.equals("null", ignoreCase = true)) {
                mobile_txt.text = serviceCenterListModel.contactMobileNumber
            } else {
                mobile_txt.text = "Not found"
            }
            if (!serviceCenterListModel.contactEmail.equals("null", ignoreCase = true)) {
                email_txt.text = serviceCenterListModel.contactEmail
            } else {
                email_txt.text = "Not found"
            }
            if (!serviceCenterListModel.averageRating.equals("null", ignoreCase = true)) {
                sst_rating.rating = serviceCenterListModel.averageRating!!.toFloat()
            } else {
                email_txt.text = "Not found"
            }
            if (!serviceCenterListModel.areaManagerName.equals("", ignoreCase = true)) {
                am_name_txt.visibility = View.VISIBLE
                am_name_txt.text = serviceCenterListModel.areaManagerName + "  (Area Manager)"
            } else {
                am_name_txt.visibility = View.GONE
                am_name_txt.text = "No name"
            }
            if (!serviceCenterListModel.areaManagerNumber!!.isEmpty()) {
                am_call_img.visibility = View.VISIBLE
                am_mobile_txt.visibility = View.VISIBLE
                am_mobile_txt.text = serviceCenterListModel.areaManagerNumber
            } else {
                am_call_img.visibility = View.GONE
                am_mobile_txt.visibility = View.GONE
                am_mobile_txt.text = "No found"
            }
            mobile_txt.setOnClickListener {
                val callIntent = Intent(Intent.ACTION_DIAL)
                callIntent.data = Uri.parse(
                    "tel:" + Uri.encode(
                        serviceCenterListModel.contactMobileNumber
                    )
                )
                callIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                context.startActivity(callIntent)
            }
            email_txt.setOnClickListener {
                val emailIntent = Intent(Intent.ACTION_SEND)
                emailIntent.type = "plain/text"
                emailIntent.putExtra(
                    Intent.EXTRA_EMAIL,
                    arrayOf(serviceCenterListModel.contactEmail)
                )
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject")
                emailIntent.putExtra(Intent.EXTRA_TEXT, "Text")
                context.startActivity(Intent.createChooser(emailIntent, "Send mail..."))
            }
            redirect_img.setOnClickListener {
                val uri =
                    "http://maps.google.com/maps?q=loc:" + serviceCenterListModel.latitude + "," + serviceCenterListModel.longitude + " (" + "Area Managet" + ")"
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
                context.startActivity(intent)
            }
            am_mobile_txt.setOnClickListener {
                val callIntent = Intent(Intent.ACTION_DIAL)
                callIntent.data = Uri.parse(
                    "tel:" + Uri.encode(
                        serviceCenterListModel.areaManagerNumber
                    )
                )
                callIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                context.startActivity(callIntent)
            }
            itemView.setOnClickListener { view: View? -> }
        }

        override fun onClick(view: View) {
            sstItemClickListener!!.onItemClick(view, layoutPosition)
        }

        init {
            itemView.setOnClickListener(this)
            redirect_img = itemView.findViewById(R.id.redirect_img)
            am_call_img = itemView.findViewById(R.id.am_call_img)
            sst_name_txt = itemView.findViewById(R.id.sst_name_txt)
            name_txt = itemView.findViewById(R.id.name_txt)
            location_txt = itemView.findViewById(R.id.location_txt)
            time_txt = itemView.findViewById(R.id.time_txt)
            mobile_txt = itemView.findViewById(R.id.mobile_txt)
            email_txt = itemView.findViewById(R.id.email_txt)
            sst_rating = itemView.findViewById(R.id.sst_rating)
            am_name_txt = itemView.findViewById(R.id.am_name_txt)
            am_mobile_txt = itemView.findViewById(R.id.am_mobile_txt)
        }
    }

    private fun setAnimation(viewToAnimate: View, position: Int) {
        if (position > lastPosition) {
            val animation = AnimationUtils.loadAnimation(
                context,
                android.R.anim.slide_in_left
            )
            viewToAnimate.startAnimation(animation)
            lastPosition = position
        }
    }

    init {
        filterList = serviceCenterListModels
        this.context = context
        this.resource = resource
        li =
            context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    }
}