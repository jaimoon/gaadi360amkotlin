package com.example.amkotlin.utils

object AppUrls {

    //var BASE_URL = "https://admin.gaadi360.com/gaadi2/api/"
    var BASE_URL = "http://test.gaadi360.com/gaadi/api/"
    //var BASE_URL = "http://192.168.32.97:8080/gaadi/api/"

    var IMAGE_URL = BASE_URL + "media/file/display/"

    var VERSION_UPDATE = "build/info/1/3"
    var SERVICETYPE = "service/type"
    var CALL_OTP = "V2/login/resend/otp?mobile="
    var PROFILE = "secure/profile/get"
    var EDIT_PROFILE = "secure/profile/update"
    var EDIT_PROFILE_PIC = "media/file/upload"
    var HELP_CENTER = "secure/help-center/get"
    var LOGIN = "login/generate/otp?mobile="
    var VERIFYOTP = "login/via/otp?mobile="
    var FCM = "secure/user/device"
    var SEND_LOCATION = "secure/location/save"
    var HISTORY = "secure/area_manager/bookings?status="
    var HISTORY_DETAIL = "secure/booking/"
    var NOTIFICATIONS = "secure/notifications"
    var READ_UNREAD = "secure/notification/"
    var CLEAR_NOTIFICATIONS = "secure/notification/all/clear"
    var NOTIFICATION_COUNT = "secure/notifications/unread-count"
    var CONCERNS = "secure/area_manager/bookingConcerns?status="
    var RESOLVE_CONCERN = "secure/booking/concern"
    var AREA_MANAGER_LIST = "secure/area-managers"
    var AREA_MANAGER_LOCATION_DETAILS = "secure/areaManager/"
    var AREA_MANAGER_LOCATION_DETAILS2 = "/location?searchDate="
    var SST_LIST = "secure/areaManager/service_centers"
    var ADD_PROSPECT = "secure/prospect/create"
    var PROSPECTS_LIST = "secure/areaManager/prospects"
}