package com.example.amkotlin.utils

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

object DateTimeUtils {
    fun utc(inputDateFormat: String?, time: String, outputDateFormat: String?): String {
        try {
            val df = SimpleDateFormat(inputDateFormat)
            val cal = Calendar.getInstance()
            val d = df.parse(time)
            cal.time = d
            cal.add(Calendar.HOUR, 5)
            cal.add(Calendar.MINUTE, 30)
            val newTime = df.format(cal.time)
            val dateFormatter =
                SimpleDateFormat(inputDateFormat)
            val date = dateFormatter.parse(newTime)
            val timeFormatter =
                SimpleDateFormat(outputDateFormat)
            return timeFormatter.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return time
    }

    fun formatDate(
        stringdate: String,
        stringdateformat: String?,
        returndateformat: String?
    ): String {
        return try {
            val date =
                SimpleDateFormat(stringdateformat).parse(stringdate)
            SimpleDateFormat(returndateformat).format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
            stringdate
        }
    }
}