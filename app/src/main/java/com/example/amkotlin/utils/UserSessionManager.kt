package com.example.amkotlin.utils

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import com.example.amkotlin.MainActivity
import com.example.amkotlin.activities.LoginActivity
import java.util.*

class UserSessionManager @SuppressLint("CommitPrefEdits") constructor(private val _context: Context) {
    private val sharedPreferences: SharedPreferences
    private val editor: SharedPreferences.Editor
    fun createDeviceId(deviceId: String?) {
        editor.putString(DEVICE_ID, deviceId)
        editor.apply()
    }

    fun checkLogin(): Boolean {
        if (isUserLoggedIn) {
            val i = Intent(_context, MainActivity::class.java)
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            i.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            _context.startActivity(i)
            return true
        }
        return false
    }

    fun createUserLoginSession(
        firstName: String?,
        username: String?,
        email: String?,
        mobile: String?,
        accessToken: String?,
        mediaSecret: String?,
        authority: String?
    ) {
        editor.putBoolean(IS_USER_LOGIN, true)
        editor.putString(FIRST_NAME, firstName)
        editor.putString(USER_NAME, username)
        editor.putString(USER_EMAIL, email)
        editor.putString(USER_MOBILE, mobile)
        editor.putString(KEY_ACCSES, accessToken)
        editor.putString(MEDIA_SECRET, mediaSecret)
        editor.putString(AUTHORITY, authority)
        editor.apply()
    }

    val userDetails: HashMap<String, String?>
        get() {
            val user =
                HashMap<String, String?>()
            user[FIRST_NAME] = sharedPreferences.getString(FIRST_NAME, null)
            user[USER_NAME] = sharedPreferences.getString(USER_NAME, null)
            user[USER_EMAIL] = sharedPreferences.getString(USER_EMAIL, null)
            user[USER_MOBILE] = sharedPreferences.getString(USER_MOBILE, null)
            user[KEY_ACCSES] = sharedPreferences.getString(KEY_ACCSES, null)
            user[MEDIA_SECRET] = sharedPreferences.getString(MEDIA_SECRET, null)
            user[AUTHORITY] = sharedPreferences.getString(AUTHORITY, null)
            return user
        }

    val isUserLoggedIn: Boolean
        get() = !sharedPreferences.getBoolean(IS_USER_LOGIN, false)

    fun logoutUser() {
        editor.clear()
        editor.apply()
        val i = Intent(_context, LoginActivity::class.java)
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        i.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        _context.startActivity(i)
    }

    companion object {
        private const val PREFER_NAME = "Gaadi360AreaManager"
        private const val IS_USER_LOGIN = "IsUserLoggedIn"
        const val DEVICE_ID = "device_id"
        const val KEY_ACCSES = "access_key"
        const val FIRST_NAME = "firstName"
        const val USER_NAME = "user_name"
        const val USER_MOBILE = "user_mobile"
        const val USER_EMAIL = "user_email"
        const val MEDIA_SECRET = "mediaSecret"
        const val AUTHORITY = "authority"
    }

    init {
        val PRIVATE_MODE = 0
        sharedPreferences =
            _context.getSharedPreferences(PREFER_NAME, PRIVATE_MODE)
        editor = sharedPreferences.edit()
    }
}