package com.example.amkotlin.interfaces

import android.view.View

interface NewHistoryItemClickListener {
    fun onItemClick(v: View?, pos: Int)
}