package com.example.amkotlin.interfaces

import android.view.View

interface AddonHistoryItemClickListener {
    fun onItemClick(v: View?, pos: Int)
}