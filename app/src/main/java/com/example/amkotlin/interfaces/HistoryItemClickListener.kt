package com.example.amkotlin.interfaces

import android.view.View

interface HistoryItemClickListener {
    fun onItemClick(v: View?, pos: Int)
}