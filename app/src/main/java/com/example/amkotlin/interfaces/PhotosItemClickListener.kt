package com.example.amkotlin.interfaces

import android.view.View

interface PhotosItemClickListener {
    fun onItemClick(v: View?, pos: Int)
}