package com.example.amkotlin.interfaces

import android.view.View

interface DeliveredHistoryItemClickListener {
    fun onItemClick(v: View?, pos: Int)
}