package com.example.amkotlin.interfaces

import android.view.View

interface SstItemClickListener {
    fun onItemClick(v: View?, pos: Int)
}