package com.example.amkotlin.interfaces

import android.view.View

interface ConcernItemClickListener {
    fun onItemClick(v: View?, pos: Int)
}