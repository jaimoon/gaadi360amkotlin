package com.example.amkotlin.interfaces

import android.view.View

interface TrackingItemClickListener {
    fun onItemClick(v: View?, pos: Int)
}