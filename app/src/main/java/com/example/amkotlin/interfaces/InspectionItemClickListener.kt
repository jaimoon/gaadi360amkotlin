package com.example.amkotlin.interfaces

import android.view.View

interface InspectionItemClickListener {
    fun onItemClick(v: View?, pos: Int)
}