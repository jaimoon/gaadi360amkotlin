package com.example.amkotlin.interfaces

import android.view.View

interface ProspectListItemClickListener {
    fun onItemClick(v: View?, pos: Int)
}