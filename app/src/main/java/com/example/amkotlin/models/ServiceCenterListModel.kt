package com.example.amkotlin.models

class ServiceCenterListModel {
    var serviceCenterId: String? = null
    var serviceCenterName: String? = null
    var contactName: String? = null
    var userId: String? = null
    var contactMobileNumber: String? = null
    var contactEmail: String? = null
    var address: String? = null
    var latitude: String? = null
    var longitude: String? = null
    var openTime: String? = null
    var closeTime: String? = null
    var averageRating: String? = null
    var deleted: String? = null
    var createdTime: String? = null
    var modifiedTime: String? = null
    var displayTime: String? = null
    var areaManagerId: String? = null
    var areaManagerName: String? = null
    var areaManagerNumber: String? = null

}