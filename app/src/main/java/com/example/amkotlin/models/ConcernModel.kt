package com.example.amkotlin.models

class ConcernModel {
    var serviceCenterName: String? = null
    var concernId: String? = null
    var bookingId: String? = null
    var concernMessage: String? = null
    var bookingDate: String? = null
    var bookingTime: String? = null
    var deliveredTime: String? = null
    var concernTime: String? = null
    var userMobile: String? = null
    var userName: String? = null
    var finalPrice: String? = null
    var status: String? = null
    var resolvedMessage: String? = null

}