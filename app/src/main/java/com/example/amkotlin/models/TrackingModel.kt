package com.example.amkotlin.models

import com.example.amkotlin.utils.DateTimeUtils.utc

class TrackingModel {
    var id: String? = null
    var userId: String? = null
    var userName: String? = null
    private lateinit var createdTime: String
    var latitude: String? = null
    var longitude: String? = null
    var location: String? = null
    var mobile: String? = null

    fun getCreatedTime(): String? {
        return createdTime
    }

    fun setCreatedTime(createdTime: String?) {
        var createdTime = createdTime
        createdTime = utc("yyyy-MM-dd HH:mm:ss", createdTime!!, "dd-MM-yyyy hh:mm a")
        this.createdTime = createdTime
    }

}