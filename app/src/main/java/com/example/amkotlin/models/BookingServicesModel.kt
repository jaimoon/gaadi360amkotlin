package com.example.amkotlin.models

class BookingServicesModel {

    var bookingId: String? = null
    var serviceId: String? = null
    var serviceName: String? = null
    var cost: String? = null
    var status: String? = null

}