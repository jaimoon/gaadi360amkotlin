package com.example.amkotlin.models

class ProspectListModel {
    var id: String? = null
    var username: String? = null
    var firstName: String? = null
    var lastName: String? = null
    var bookingDate: String? = null
    var comments: String? = null
    var email: String? = null
    var createdTime: String? = null
    var areaManagerName: String? = null
    var areaManagerNumber: String? = null
    var type: String? = null

}