package com.example.amkotlin

import android.Manifest
import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.Typeface
import android.location.*
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import com.android.volley.*
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.amkotlin.activities.*
import com.example.amkotlin.utils.*
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.GoogleMap.OnCameraIdleListener
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.net.PlacesClient
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.material.snackbar.Snackbar
import com.nex3z.notificationbadge.NotificationBadge
import com.taishi.flipprogressdialog.FlipProgressDialog
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.util.*

class MainActivity : FragmentActivity(), View.OnClickListener,
    OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener, LocationListener {
    private var exit = false
    var badge: NotificationBadge? = null
    var versionDialog: AlertDialog? = null
    var version = 0
    var regular: Typeface? = null
    var bold: Typeface? = null
    private var checkInternet = false
    var userSessionManager: UserSessionManager? = null
    var accessToken: String? = null
    var name: String? = null
    var authority: String? = null
    var notification_img: ImageView? = null
    var location_img: ImageView? = null
    var booking_ll: LinearLayout? = null
    var sst_ll: LinearLayout? = null
    var concern_ll: LinearLayout? = null
    var user_details_ll: LinearLayout? = null
    var toolbar_title: TextView? = null
    var share_txt: TextView? = null
    var settings_txt: TextView? = null
    var booking_txt: TextView? = null
    var sst_txt: TextView? = null
    var concern_txt: TextView? = null
    var user_details_txt: TextView? = null
    var location_txt: TextView? = null
    var area_txt: TextView? = null
    var address_txt: TextView? = null
    var track_btn: Button? = null
    var latitude = 0.0
    var longitude = 0.0
    var locationManager: LocationManager? = null
    var criteria: Criteria? = null
    var bestProvider: String? = null
    var locality: String? = null
    var location: Location? = null

    //Geocoder geocoder;
    private var isGPS = false
    var mapFragment: SupportMapFragment? = null
    private var mMap: GoogleMap? = null
    private var onCameraIdleListener: OnCameraIdleListener? = null
    var mLocationRequest: LocationRequest? = null
    var mGoogleApiClient: GoogleApiClient? = null
    var mLastLocation: Location? = null
    var mCurrLocationMarker: Marker? = null
    var latitute = 0.0
    var longitiu = 0.0
    var addresses: List<Address>? = null
    var geocoder: Geocoder? = null
    var latitude1: String? = null
    var longitude1: String? = null
    var placesClient: PlacesClient? = null
    var latLng: LatLng? = null
    var sendLat: String? = null
    var sendLng: String? = null
    var lm: LocationManager? = null

    /*ProgressDialog*/
    var imageList: MutableList<Int> = ArrayList()
    var flipProgressDialog: FlipProgressDialog? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        imageList.add(R.mipmap.ic_launcher)
        flipProgressDialog = FlipProgressDialog()
        flipProgressDialog!!.setImageList(imageList)
        flipProgressDialog!!.setCanceledOnTouchOutside(true)
        flipProgressDialog!!.setDimAmount(0.8f) //0.0f
        flipProgressDialog!!.setBackgroundColor(Color.parseColor("#00FFFFFF"))
        flipProgressDialog!!.setBackgroundAlpha(0.2f)
        flipProgressDialog!!.setBorderStroke(0)
        flipProgressDialog!!.setBorderColor(-1)
        flipProgressDialog!!.setCornerRadius(16)
        flipProgressDialog!!.setImageSize(200)
        flipProgressDialog!!.setImageMargin(10)
        flipProgressDialog!!.setOrientation("rotationY")
        flipProgressDialog!!.setDuration(600)
        flipProgressDialog!!.setStartAngle(0.0f)
        flipProgressDialog!!.setEndAngle(180.0f)
        flipProgressDialog!!.setMinAlpha(0.0f)
        flipProgressDialog!!.setMaxAlpha(1.0f)
        regular = Typeface.createFromAsset(assets, "proxima_nova_regular.otf")
        bold = Typeface.createFromAsset(assets, "proxima_nova_bold.otf")
        checkInternet = NetworkChecking.isConnected(this)
        userSessionManager = UserSessionManager(this)
        val userDetails: HashMap<String, String?>? = userSessionManager?.userDetails
        accessToken = userDetails?.get(UserSessionManager.KEY_ACCSES)
        name = userDetails?.get(UserSessionManager.FIRST_NAME)
        authority = userDetails?.get(UserSessionManager.AUTHORITY)
        Log.d("AUTHORITY", authority)
        toolbar_title = findViewById(R.id.toolbar_title)
        toolbar_title?.setTypeface(bold)
        toolbar_title?.setText("Welcome \n$name")
        share_txt = findViewById(R.id.share_img)
        share_txt?.setOnClickListener(this)
        settings_txt = findViewById(R.id.settings_img)
        settings_txt?.setOnClickListener(this)
        badge = findViewById(R.id.badge)
        notification_img = findViewById(R.id.notification_img)
        notification_img?.setOnClickListener(this)
        checkVersionUpdate()
        notificationCount
        track_btn = findViewById(R.id.track_btn)
        track_btn?.setTypeface(bold)
        track_btn?.setOnClickListener(this)
        if (authority.equals("CITY_MANAGER", ignoreCase = true)) {
            track_btn?.setVisibility(View.VISIBLE)
        } else {
            track_btn?.setVisibility(View.GONE)
        }
        booking_ll = findViewById(R.id.booking_ll)
        booking_ll?.setOnClickListener(this)
        sst_ll = findViewById(R.id.sst_ll)
        sst_ll?.setOnClickListener(this)
        concern_ll = findViewById(R.id.concern_ll)
        concern_ll?.setOnClickListener(this)
        user_details_ll = findViewById(R.id.user_details_ll)
        user_details_ll?.setOnClickListener(this)
        booking_txt = findViewById(R.id.booking_txt)
        booking_txt?.setTypeface(bold)
        booking_txt?.setOnClickListener(this)
        sst_txt = findViewById(R.id.sst_txt)
        sst_txt?.setTypeface(bold)
        sst_txt?.setOnClickListener(this)
        concern_txt = findViewById(R.id.concern_txt)
        concern_txt?.setTypeface(bold)
        concern_txt?.setOnClickListener(this)
        user_details_txt = findViewById(R.id.user_details_txt)
        user_details_txt?.setTypeface(bold)
        user_details_txt?.setOnClickListener(this)
        location_txt = findViewById(R.id.location_txt)
        location_txt?.setTypeface(regular)
        address_txt = findViewById(R.id.address_txt)
        address_txt?.setTypeface(regular)
        area_txt = findViewById(R.id.area_txt)
        area_txt?.setTypeface(bold)
        location_img = findViewById(R.id.location_img)
        location_img?.setOnClickListener(this)
    }

    private fun checkLocationPermission(): Boolean {
        val locationPermission = ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.ACCESS_FINE_LOCATION
        )
        val listPermissionsNeeded: MutableList<String> =
            ArrayList()
        if (locationPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION)
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(
                this, listPermissionsNeeded.toTypedArray(),
                REQUEST_ID_MULTIPLE_PERMISSIONS
            )
            return false
        }
        return true
    }

    val currentLocation: Unit
        get() {
            locationManager =
                this.getSystemService(Context.LOCATION_SERVICE) as LocationManager
            criteria = Criteria()
            bestProvider = locationManager!!.getBestProvider(criteria, true).toString()
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(
                        Manifest.permission.ACCESS_COARSE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    currentLocation
                    return
                }
            }
            location = locationManager!!.getLastKnownLocation(bestProvider)
            if (location != null) {
                latitude = location!!.latitude
                longitude = location!!.longitude
            }
            geocoder = Geocoder(this@MainActivity)
            try {
                val addressList =
                    geocoder!!.getFromLocation(latitude, longitude, 1)
                if (addressList != null && addressList.size > 0) {
                    latitude = addressList[0].latitude
                    longitude = addressList[0].longitude
                    locality = addressList[0].getAddressLine(0)
                    if (!locality?.isEmpty()!!) {
                        Log.d(
                            "LOCATION",
                            "Locality ->$latitude\n$longitude\n$locality"
                        )
                        location_txt!!.text = locality
                        sendLocation()
                    } else {
                        GlobalCalls.showToast("Something went wrong..!", this@MainActivity)
                    }
                }
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }

    private fun sendLocation() {
        val url: String = AppUrls.BASE_URL + AppUrls.SEND_LOCATION
        val jsonObject = JSONObject()
        try {
            jsonObject.put("latitude", latitute)
            jsonObject.put("longitude", longitiu)
            jsonObject.put("location", locality)
        } catch (e: Exception) {
        }
        Log.d("JSONOBJ", jsonObject.toString())
        val request: JsonObjectRequest = object : JsonObjectRequest(
            Method.POST,
            url,
            jsonObject,
            Response.Listener { response ->
                try {
                    val jsonObject = JSONObject(response.toString())
                    val status = jsonObject.getString("status")
                    val message = jsonObject.getString("message")
                    flipProgressDialog!!.dismiss()
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            },
            Response.ErrorListener { error -> error.printStackTrace() }) {
            override fun getHeaders(): Map<String, String> {
                val headers: MutableMap<String, String> =
                    HashMap()
                headers["Authorization"] = "Bearer $accessToken"
                return headers
            }
        }
        val requestQueue = Volley.newRequestQueue(this@MainActivity)
        requestQueue.add(request)
    }

    private fun checkVersionUpdate() {
        try {
            val pInfo =
                this@MainActivity.packageManager.getPackageInfo(packageName, 0)
            version = pInfo.versionCode
            Log.d("VERSION", "" + version)
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
        val url: String = AppUrls.BASE_URL + AppUrls.VERSION_UPDATE
        val stringRequest = StringRequest(
            Request.Method.GET, url, Response.Listener { response ->
                try {
                    val jsonObject = JSONObject(response)
                    val id = jsonObject.optString("id")
                    val platformId = jsonObject.optString("platformId")
                    val currentVersionCode = jsonObject.optInt("currentVersionCode")
                    val mandatoryUpdateVersionCode =
                        jsonObject.optInt("mandatoryUpdateVersionCode")
                    val versionName = jsonObject.optString("versionName")
                    val mandatory = jsonObject.optString("mandatory")
                    val status = jsonObject.optString("status")
                    val createdTime = jsonObject.optString("createdTime")
                    val modifiedTime = jsonObject.optString("modifiedTime")
                    if (mandatoryUpdateVersionCode > version) {
                        versionUpdateAlert()
                    } else {
                        if (currentVersionCode > version) {
                            optionalVersionUpdateAlert()
                        }
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            },
            Response.ErrorListener { error: VolleyError? ->
                if (error is TimeoutError || error is NoConnectionError) {
                    GlobalCalls.showToast("Connection Not Found..!", this@MainActivity)
                } else if (error is AuthFailureError) {
                    GlobalCalls.showToast("User Not Found..!", this@MainActivity)
                } else if (error is ServerError) {
                    GlobalCalls.showToast("Server Not Found..!", this@MainActivity)
                } else if (error is NetworkError) {
                    GlobalCalls.showToast("Network Not Found..!", this@MainActivity)
                } else if (error is ParseError) {
                    GlobalCalls.showToast("Please Try Later..!", this@MainActivity)
                }
            }
        )
        val requestQueue = Volley.newRequestQueue(this@MainActivity)
        requestQueue.add(stringRequest)
    }

    fun versionUpdateAlert() {
        val builder =
            AlertDialog.Builder(this@MainActivity)
        val inflater = this@MainActivity.layoutInflater
        val dialog_layout: View =
            inflater.inflate(R.layout.version_update_dialog, null)
        val title_txt = dialog_layout.findViewById<TextView>(R.id.title_txt)
        title_txt.setTypeface(bold)
        val msg_txt = dialog_layout.findViewById<TextView>(R.id.msg_txt)
        msg_txt.setTypeface(regular)
        val update_btn =
            dialog_layout.findViewById<Button>(R.id.update_btn)
        update_btn.setTypeface(bold)
        update_btn.setOnClickListener {
            if (checkInternet) {
                val appPackageName = packageName
                try {
                    startActivity(
                        Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse("market://details?id=$appPackageName")
                        )
                    )
                } catch (anfe: ActivityNotFoundException) {
                    startActivity(
                        Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")
                        )
                    )
                }
            } else {
                val snackbar = Snackbar.make(
                    window.decorView,
                    "Check Internet Connection",
                    Snackbar.LENGTH_LONG
                )
                snackbar.show()
            }
        }
        builder.setView(dialog_layout)
        versionDialog = builder.create()
        versionDialog!!.setCancelable(false)
        versionDialog!!.show()
    }

    fun optionalVersionUpdateAlert() {
        val builder =
            AlertDialog.Builder(this@MainActivity)
        val inflater = this@MainActivity.layoutInflater
        val dialog_layout: View =
            inflater.inflate(R.layout.version_update_dialog, null)
        val title_txt = dialog_layout.findViewById<TextView>(R.id.title_txt)
        title_txt.setTypeface(bold)
        val msg_txt = dialog_layout.findViewById<TextView>(R.id.msg_txt)
        msg_txt.setTypeface(regular)
        val update_btn =
            dialog_layout.findViewById<Button>(R.id.update_btn)
        update_btn.setTypeface(bold)
        update_btn.setOnClickListener {
            if (checkInternet) {
                val appPackageName = packageName
                try {
                    startActivity(
                        Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse("market://details?id=$appPackageName")
                        )
                    )
                } catch (anfe: ActivityNotFoundException) {
                    startActivity(
                        Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")
                        )
                    )
                }
            } else {
                val snackbar = Snackbar.make(
                    window.decorView,
                    "Check Internet Connection",
                    Snackbar.LENGTH_LONG
                )
                snackbar.show()
            }
        }
        builder.setView(dialog_layout).setNegativeButton(
            "CANCEL"
        ) { dialogInterface: DialogInterface, i: Int -> dialogInterface.dismiss() }
        versionDialog = builder.create()
        versionDialog!!.setCancelable(false)
        versionDialog!!.show()
    }

    private val notificationCount: Unit
        private get() {
            val url: String = AppUrls.BASE_URL + AppUrls.NOTIFICATION_COUNT
            val stringRequest: StringRequest =
                object : StringRequest(
                    Method.GET,
                    url,
                    Response.Listener { response ->
                        try {
                            val jsonObject = JSONObject(response)
                            if (jsonObject.length() != 0) {
                                badge!!.visibility = View.VISIBLE
                                val notificationUnreadCount =
                                    jsonObject.optString("notificationUnreadCount")
                                badge!!.setNumber(notificationUnreadCount.toInt())
                            } else {
                                badge!!.visibility = View.GONE
                            }
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    },
                    Response.ErrorListener { error ->
                        if (error is TimeoutError || error is NoConnectionError) {
                        } else if (error is AuthFailureError) {
                        } else if (error is ServerError) {
                        } else if (error is NetworkError) {
                        } else if (error is ParseError) {
                        }
                    }
                ) {
                    override fun getHeaders(): Map<String, String> {
                        val headers: MutableMap<String, String> =
                            HashMap()
                        headers["Authorization"] = "Bearer $accessToken"
                        return headers
                    }
                }
            val requestQueue = Volley.newRequestQueue(this@MainActivity)
            requestQueue.add(stringRequest)
        }

    override fun onClick(v: View) {
        if (v === share_txt) {
            if (checkInternet) {
                val sharingIntent = Intent(Intent.ACTION_SEND)
                sharingIntent.type = "text/plain"
                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Gaadi360")
                sharingIntent.putExtra(
                    Intent.EXTRA_TEXT,
                    """
                        A two-wheeler service that's sure to give you more SMILEAGE.
                        Enjoy unparalleled service with Gaadi360.  
                        Download at app.gaadi360.com
                        """.trimIndent()
                )
                startActivity(Intent.createChooser(sharingIntent, "Share via"))
            } else {
                val snackbar = Snackbar.make(
                    window.decorView,
                    "Check Internet Connection",
                    Snackbar.LENGTH_LONG
                )
                snackbar.show()
            }
        }
        if (v === settings_txt) {
            if (checkInternet) {
                val intent = Intent(this@MainActivity, SettingActivity::class.java)
                startActivity(intent)
                finish()
            } else {
                val snackbar = Snackbar.make(
                    window.decorView,
                    "Check Internet Connection",
                    Snackbar.LENGTH_LONG
                )
                snackbar.show()
            }
        }
        if (v === notification_img) {
            if (checkInternet) {
                val intent = Intent(this@MainActivity, NotificationActivity::class.java)
                startActivity(intent)
                finish()
            } else {
                val snackbar = Snackbar.make(
                    window.decorView,
                    "Check Internet Connection",
                    Snackbar.LENGTH_LONG
                )
                snackbar.show()
            }
        }
        if (v === track_btn) {
            if (checkInternet) {
                val intent = Intent(this@MainActivity, TrackingActivity::class.java)
                startActivity(intent)
            } else {
                val snackbar = Snackbar.make(
                    window.decorView,
                    "Check Internet Connection",
                    Snackbar.LENGTH_LONG
                )
                snackbar.show()
            }
        }
        if (v === booking_ll || v === booking_txt) {
            if (checkInternet) {
                val intent = Intent(this@MainActivity, TodayHistoryActivity::class.java)
                startActivity(intent)
            } else {
                val snackbar = Snackbar.make(
                    window.decorView,
                    "Check Internet Connection",
                    Snackbar.LENGTH_LONG
                )
                snackbar.show()
            }
        }
        if (v === concern_ll || v === concern_txt) {
            if (checkInternet) {
                val intent = Intent(this@MainActivity, ConcernListActivity::class.java)
                startActivity(intent)
            } else {
                val snackbar = Snackbar.make(
                    window.decorView,
                    "Check Internet Connection",
                    Snackbar.LENGTH_LONG
                )
                snackbar.show()
            }
        }
        if (v === sst_ll || v === sst_txt) {
            if (checkInternet) {
                val intent =
                    Intent(this@MainActivity, ServiceCenterListActivity::class.java)
                startActivity(intent)
            } else {
                val snackbar = Snackbar.make(
                    window.decorView,
                    "Check Internet Connection",
                    Snackbar.LENGTH_LONG
                )
                snackbar.show()
            }
        }
        if (v === user_details_ll || v === user_details_txt) {
            if (checkInternet) {
                val intent = Intent(this@MainActivity, ProspectListActivity::class.java)
                startActivity(intent)
            } else {
                val snackbar = Snackbar.make(
                    window.decorView,
                    "Check Internet Connection",
                    Snackbar.LENGTH_LONG
                )
                snackbar.show()
            }
        }
        if (v === location_img) {
            currentLocation
        }
    }

    override fun onResume() {
        super.onResume()
        checkLocationPermission()
        flipProgressDialog!!.show(fragmentManager, "")
        flipProgressDialog!!.isCancelable = false

        /*new GpsUtils(this).turnGPSOn(new GpsUtils.onGpsListener() {
            @Override
            public void gpsStatus(boolean isGPSEnable) {

                isGPS = isGPSEnable;

                getCurrentLocation();
            }
        });*/

        /*GPSTracker gpsTracker = new GPSTracker(this);

        if (gpsTracker.getIsGPSTrackingEnabled()) {
            String stringLatitude = String.valueOf(gpsTracker.latitude);
            String stringLongitude = String.valueOf(gpsTracker.longitude);
            String country = gpsTracker.getCountryName(this);
            String city = gpsTracker.getLocality(this);
            String postalCode = gpsTracker.getPostalCode(this);
            String addressLine = gpsTracker.getAddressLine(this);

            location_txt.setText(stringLatitude + "\n" + stringLongitude + "\n" + addressLine + "\n" + city + "\n" + country + "\n" + postalCode);
        } else {

            gpsTracker.showSettingsAlert();
        }*/
        val apiKey = getString(R.string.googleapikey)
        if (!Places.isInitialized()) {
            Places.initialize(
                applicationContext,
                apiKey
            )
        }
        placesClient = Places.createClient(this)
        geocoder = Geocoder(this, Locale.getDefault())
        mapFragment = supportFragmentManager?.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment!!.getMapAsync(this)
        lm = this.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        GpsUtils(this).turnGPSOn(object : GpsUtils.onGpsListener {
            override fun gpsStatus(isGPSEnable: Boolean) {
                // turn on GPS
                isGPS = isGPSEnable
            }
        })
        configureCameraIdle()
    }

    override fun onBackPressed() {
        if (exit) {
            val intent = Intent(Intent.ACTION_MAIN)
            intent.addCategory(Intent.CATEGORY_HOME)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            moveTaskToBack(true)
        } else {
            GlobalCalls.showToast("Press Back again to Exit.!", this@MainActivity)
            exit = true
            Handler().postDelayed({ exit = false }, 2 * 1000.toLong())
        }
    }

    private fun configureCameraIdle() {
        onCameraIdleListener = OnCameraIdleListener {
            latLng = mMap!!.cameraPosition.target
            try {
                val geocoder = Geocoder(this@MainActivity)
                val addressList = geocoder.getFromLocation(
                    latLng?.latitude!!,
                    latLng?.longitude!!, 1
                )
                if (addressList != null && addressList.size > 0) {
                    val address = addressList[0]
                    locality = addressList[0].getAddressLine(0)
                    val addr = locality?.split(",", 2.toString())?.toTypedArray()
                    sendLat = latLng?.latitude.toString()
                    sendLng = latLng?.longitude.toString()
                    val country = addressList[0].countryName
                    val state = addressList[0].adminArea
                    val city = addressList[0].locality
                    val area = addressList[0].subLocality
                    val pincode = addressList[0].postalCode
                    latitute = addressList[0].latitude
                    longitiu = addressList[0].longitude
                    if (area != null) {
                        area_txt!!.text = area
                    } else {
                        area_txt!!.text = "Select Location"
                    }
                    address_txt!!.text = locality
                    Log.d(
                        "LOCATION1",
                        """
                            ${latLng?.latitude}
                            ${latLng?.longitude}
                            $locality
                            """.trimIndent()
                    )
                    if (!locality?.isEmpty()!!) {
                        Log.d("LOCALITY", locality)
                        sendLocation()
                    }
                }
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                buildGoogleApiClient()
                mMap!!.isMyLocationEnabled = true
            }
        } else {
            buildGoogleApiClient()
            mMap!!.isMyLocationEnabled = true
        }
        mMap!!.setOnCameraIdleListener(onCameraIdleListener)
    }

    @Synchronized
    protected fun buildGoogleApiClient() {
        mGoogleApiClient = GoogleApiClient.Builder(this)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .addApi(LocationServices.API)
            .build()
        mGoogleApiClient?.connect()
    }

    override fun onConnected(bundle: Bundle?) {
        mLocationRequest = LocationRequest()
        mLocationRequest!!.interval = 1000
        mLocationRequest!!.fastestInterval = 1000
        mLocationRequest!!.priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
            == PackageManager.PERMISSION_GRANTED
        ) {
            LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient,
                mLocationRequest,
                this
            )
        }
    }

    override fun onConnectionSuspended(i: Int) {}
    override fun onLocationChanged(location: Location) {
        mLastLocation = location
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker!!.remove()
        }
        val latLng = LatLng(location.latitude, location.longitude)
        val markerOptions = MarkerOptions()
        markerOptions.position(latLng)
        mMap!!.moveCamera(CameraUpdateFactory.newLatLng(latLng))
        mMap!!.animateCamera(CameraUpdateFactory.zoomTo(19f))
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this)
        }
        Log.d(
            "LOCATION2",
            """
                ${latLng.latitude}
                ${latLng.longitude}
                $locality
                """.trimIndent()
        )
    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {}
    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                val place =
                    Autocomplete.getPlaceFromIntent(data!!)
                var stringlat = place.latLng.toString()
                val name: CharSequence? = place.name
                val address: CharSequence? = place.address
                stringlat = stringlat.substring(stringlat.indexOf("(") + 1)
                stringlat = stringlat.substring(0, stringlat.indexOf(")"))
                val latValue = stringlat.split(",").toTypedArray()[0]
                latitude1 = latValue
                val lngValue = stringlat.split(",").toTypedArray()[1]
                longitude1 = lngValue
                sendLat = latitude1
                sendLng = longitude1
                val lat = java.lang.Double.valueOf(latitude1!!)
                val lng = java.lang.Double.valueOf(longitude1!!)
                val fulladd = address.toString()
                val addr: Array<String?> = fulladd.split(",", 2.toString()).toTypedArray()
                try {
                    addresses = geocoder!!.getFromLocation(lat, lng, 1)
                    Log.d("LOXXX", addresses?.get(0)?.getAddressLine(0))
                    val addLine = addresses?.get(0)?.getAddressLine(0)
                    val city = addresses?.get(0)?.locality
                    val area = addresses?.get(0)?.subLocality
                    val state = addresses?.get(0)?.adminArea
                    val country = addresses?.get(0)?.countryName
                    val postalCode = addresses?.get(0)?.postalCode
                    val addLineTwo = addresses?.get(0)?.thoroughfare
                    val addLineOne = addresses?.get(0)?.featureName
                    if (area != null) {
                        if (addr[1] != null && area != null && addr[1]!!.contains(area)) {
                            addr[1] = addr[1]!!.replace(area, " ")
                        }
                    }
                    if (city != null) {
                        if (addr[1]!!.contains(city)) {
                            addr[1] = addr[1]!!.replace(city, " ")
                        }
                    }
                    if (state != null) {
                        if (addr[1]!!.contains(state)) {
                            addr[1] = addr[1]!!.replace(state, " ")
                        }
                    }
                    if (postalCode != null) {
                        if (addr[1]!!.contains(postalCode)) {
                            addr[1] = addr[1]!!.replace(postalCode, " ")
                        }
                    }
                    if (country != null) {
                        if (addr[1]!!.contains(country)) {
                            addr[1] = addr[1]!!.replace(country, " ")
                        }
                    }
                    location_txt!!.text = area
                    address_txt!!.text = addr[1]
                    addr[1] = LinkedHashSet(
                        Arrays.asList(
                            *addr[1]!!.split("\\s").toTypedArray()
                        )
                    ).toString().replace("[\\[\\],]".toRegex(), "")
                    if (mCurrLocationMarker != null) {
                        mCurrLocationMarker!!.remove()
                    }
                    val latLng = LatLng(latitude1!!.toDouble(), longitude1!!.toDouble())
                    val markerOptions = MarkerOptions()
                    markerOptions.position(latLng)
                    mMap!!.moveCamera(CameraUpdateFactory.newLatLng(latLng))
                    mMap!!.animateCamera(CameraUpdateFactory.zoomTo(19f))
                    if (mGoogleApiClient != null) {
                        LocationServices.FusedLocationApi.removeLocationUpdates(
                            mGoogleApiClient,
                            this
                        )
                    }
                } catch (e: IOException) {
                    e.printStackTrace()
                }
                Log.d(
                    "LOCATION3",
                    """
                        ${latLng!!.latitude}
                        ${latLng!!.longitude}
                        $locality
                        """.trimIndent()
                )
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                val status =
                    Autocomplete.getStatusFromIntent(data!!)
                GlobalCalls.showToast(status.statusMessage, this@MainActivity)
            } else if (resultCode == Activity.RESULT_CANCELED) {
            }
        }
    }

    companion object {
        const val REQUEST_ID_MULTIPLE_PERMISSIONS = 1
        const val AUTOCOMPLETE_REQUEST_CODE = 22
    }
}

